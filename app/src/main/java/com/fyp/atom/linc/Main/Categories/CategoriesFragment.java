package com.fyp.atom.linc.Main.Categories;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Models.Category;
import com.fyp.atom.linc.Utils.CategoryLab;

import java.util.ArrayList;
import java.util.List;

public class CategoriesFragment extends Fragment {
    View view;
    Bundle mSavedInstanceState;
    private GridLayoutManager gridLayoutManager;
    private RecyclerView.Adapter adapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<Category> categoriesList;



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_categories, container, false);
        //This is basically never used, it's just to use getData from onScrolled function
        mSavedInstanceState = savedInstanceState;
        //Initializing our categoriesList list
        categoriesList = new ArrayList<>();


        CategoryLab categoryLab = CategoryLab.get(getActivity());
        categoriesList = categoryLab.getCategories();

//        Log.d("tttt", categoriesList.get(2).getCategoryName());
        //Makes gridview for child and listview for parent
        gridLayoutManager = new GridLayoutManager(getActivity(), 2){
//            @Override
//            public boolean canScrollVertically() {
////                return false;
//            }
        };

        //Initializing Views
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView_courses);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);

        //THIS IS if we want it as a list (not grid of 2)
//        layoutManager = new LinearLayoutManager(getActivity());


        //Adding an scroll change listener to recyclerview
//        recyclerView.addOnScrollListener(rVOnScrollListener);

        //initializing our adapter
        adapter = new CategoriesAdapter(categoriesList, getActivity());

        //Adding adapter to recyclerview
        recyclerView.setAdapter(adapter);



        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // crashes in beginning of app sometimes NullPointerException onSaveInstanceState
        // I REMOVED IT, TURNS OUT WE DON'T NEED IT I GUESS?
//        ((ExpandableAdapter) mCategoryRecyclerView.getAdapter()).onSaveInstanceState(outState);
    }


    private void reloadFragment(){
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }

}
