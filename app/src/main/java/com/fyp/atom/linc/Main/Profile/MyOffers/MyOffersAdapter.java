package com.fyp.atom.linc.Main.Profile.MyOffers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fyp.atom.linc.Main.Profile.MyOffers.MyOffersDetails.RequestersActivity;
import com.fyp.atom.linc.Main.StudentFeed.OfferDetails.OfferDetailsActivity;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class MyOffersAdapter extends RecyclerView.Adapter<MyOffersAdapter.ViewHolder> {

    private Context context;

//    ImageButton retryButtonDiscount = null;

    //List to store all superheroes
    List<Offer> offersList;

    //Constructor of this class
    public MyOffersAdapter(List<Offer> offersList, Context context) {
        super();
        this.offersList = offersList;
        this.context = context;
        Log.d("offer", "MyOffersAdapter");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_student_feed, parent, false);

        Log.d("offer", "yo");
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //Getting the particular item from the list
        final Offer offer = offersList.get(position);

        holder.courseNameTextView.setText(offer.getCourse().getName());
        holder.nameTextView.setText(offer.getUser().getName());
        setRatingImage(holder, offer);
        setCategoryImage(holder, offer);
        holder.responseTimeTextView.setText("Response Time: "+offer.getUser().getUserResponse()+"h");
        holder.reliabilityTextView.setText("Reliability: "+offer.getUser().getUserReliability()+"%");
        holder.titleTextView.setText(offer.getTitle());
        holder.priceTextView.setText("" + offer.getPrice());
        holder.timeSlotTextView.setText(offer.getDateStr());
        setTags(holder, offer);


        String logoUrl = "http://kcapplications.com/Linc/images/" + offer.getUser().getUserEmail() +"_logo.jpg";
        Log.d("logoooo", logoUrl);
        Picasso.with(context)
                .load(logoUrl)
                .placeholder(R.drawable.placeholder)
                .into(holder.profileImageView);

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(context, RequestersActivity.class);
                myIntent.putExtra("offer", offer);
                context.startActivity(myIntent);
                ((Activity) context).overridePendingTransition(0, 0);
            }
        });

    }

    @Override
    public int getItemCount() {
        return offersList.size();
    }

    public void setRatingImage(MyOffersAdapter.ViewHolder myHolder, Offer offer){
        String uriStarOn = "@drawable/star_on";  // where myresource (without the extension) is the file
        int imageResourceOn = context.getResources().getIdentifier(uriStarOn, null, context.getPackageName());
        Drawable starOn = context.getResources().getDrawable(imageResourceOn);

        String uriStarOff = "@drawable/star_off";  // where myresource (without the extension) is the file
        int imageResourceOff = context.getResources().getIdentifier(uriStarOff, null, context.getPackageName());
        Drawable starOff = context.getResources().getDrawable(imageResourceOff);

        if(offer.getUser().getUserRating() == 0){
            myHolder.starOneImageView.setImageDrawable(starOff);
            myHolder.starTwoImageView.setImageDrawable(starOff);
            myHolder.starThreeImageView.setImageDrawable(starOff);
            myHolder.starFourImageView.setImageDrawable(starOff);
            myHolder.starFiveImageView.setImageDrawable(starOff);
        } else if(offer.getUser().getUserRating() == 1){
            myHolder.starOneImageView.setImageDrawable(starOn);
            myHolder.starTwoImageView.setImageDrawable(starOff);
            myHolder.starThreeImageView.setImageDrawable(starOff);
            myHolder.starFourImageView.setImageDrawable(starOff);
            myHolder.starFiveImageView.setImageDrawable(starOff);
        } else if(offer.getUser().getUserRating() == 2){
            myHolder.starOneImageView.setImageDrawable(starOn);
            myHolder.starTwoImageView.setImageDrawable(starOn);
            myHolder.starThreeImageView.setImageDrawable(starOff);
            myHolder.starFourImageView.setImageDrawable(starOff);
            myHolder.starFiveImageView.setImageDrawable(starOff);
        }else if(offer.getUser().getUserRating() == 3){
            myHolder.starOneImageView.setImageDrawable(starOn);
            myHolder.starTwoImageView.setImageDrawable(starOn);
            myHolder.starThreeImageView.setImageDrawable(starOn);
            myHolder.starFourImageView.setImageDrawable(starOff);
            myHolder.starFiveImageView.setImageDrawable(starOff);
        }else if(offer.getUser().getUserRating() == 4){
            myHolder.starOneImageView.setImageDrawable(starOn);
            myHolder.starTwoImageView.setImageDrawable(starOn);
            myHolder.starThreeImageView.setImageDrawable(starOn);
            myHolder.starFourImageView.setImageDrawable(starOn);
            myHolder.starFiveImageView.setImageDrawable(starOff);
        }else if(offer.getUser().getUserRating() == 4){
            myHolder.starOneImageView.setImageDrawable(starOn);
            myHolder.starTwoImageView.setImageDrawable(starOn);
            myHolder.starThreeImageView.setImageDrawable(starOn);
            myHolder.starFourImageView.setImageDrawable(starOn);
            myHolder.starFiveImageView.setImageDrawable(starOn);
        }
    }

    public void setCategoryImage(MyOffersAdapter.ViewHolder myHolder, Offer offer){
        String categoryBarImgName = offer.getCategory().getCategoryName();
        categoryBarImgName = categoryBarImgName.toLowerCase() + "_bar";

        String uri = "@drawable/"+categoryBarImgName;  // where myresource (without the extension) is the file
        int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
        Drawable categoryBar = context.getResources().getDrawable(imageResource);
        myHolder.categoryImageView.setBackground(categoryBar);

    }

    public void  setTags(MyOffersAdapter.ViewHolder myHolder, Offer offer){
        String tags = "#" + offer.getTagsList().get(0).getName();
        for (int i = 1; i < offer.getTagsList().size(); i++) {
            tags = tags + " #" + offer.getTagsList().get(i).getName();
        }
        myHolder.tagsTextView.setText(tags);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public CardView card;

        public ImageView profileImageView;
        public ImageView starOneImageView;
        public ImageView starTwoImageView;
        public ImageView starThreeImageView;
        public ImageView starFourImageView;
        public ImageView starFiveImageView;
        public ImageView categoryImageView;

        public TextView courseNameTextView;
        public TextView nameTextView;
        public TextView responseTimeTextView;
        public TextView reliabilityTextView;
        public TextView titleTextView;
        public TextView descriptionTextView;
        public TextView priceTextView;
        public TextView timeSlotTextView;
        public TextView tagsTextView;



        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);

            Log.d("offer", "ViewHolder");

            card = (CardView) itemView.findViewById(R.id.card);

            profileImageView = (ImageView) itemView.findViewById(R.id.profileImageView);
            starOneImageView = (ImageView) itemView.findViewById(R.id.starOneImageView);
            starTwoImageView = (ImageView) itemView.findViewById(R.id.starTwoImageView);
            starThreeImageView = (ImageView) itemView.findViewById(R.id.starThreeImageView);
            starFourImageView = (ImageView) itemView.findViewById(R.id.starFourImageView);
            starFiveImageView = (ImageView) itemView.findViewById(R.id.starFiveImageView);
            categoryImageView = (ImageView) itemView.findViewById(R.id.categoryImageView);

            courseNameTextView = (TextView) itemView.findViewById(R.id.courseNameTextView);
            nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
            responseTimeTextView = (TextView) itemView.findViewById(R.id.responseTimeTextView);
            reliabilityTextView = (TextView) itemView.findViewById(R.id.reliabilityTextView);
            titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
            descriptionTextView = (TextView) itemView.findViewById(R.id.descriptionTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.priceTextView);
            timeSlotTextView = (TextView) itemView.findViewById(R.id.timeSlotTextView);
            tagsTextView = (TextView) itemView.findViewById(R.id.tagsTextView);
        }
    }
}