package com.fyp.atom.linc.Models;

import java.io.Serializable;

/**
 * Created by ATOM on 5/3/2017.
 */

public class Requester implements Serializable {

    public User getRequester() {
        return requester;
    }

    public void setRequester(User requester) {
        this.requester = requester;
    }

    User requester;

    public String getOfferingCredit() {
        return offeringCredit;
    }

    public void setOfferingCredit(String offeringCredit) {
        this.offeringCredit = offeringCredit;
    }

    String offeringCredit;


}
