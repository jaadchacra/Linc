package com.fyp.atom.linc.Unused.TeacherFeed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fyp.atom.linc.Main.StudentFeed.OfferDetails.OfferDetailsActivity;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.R;

import java.util.List;


public class TeacherFeedAdapter extends RecyclerView.Adapter<TeacherFeedAdapter.ViewHolder> {

    private Context context;

//    ImageButton retryButtonDiscount = null;

    //List to store all superheroes
    List<Offer> offersList;

    //Constructor of this class
    public TeacherFeedAdapter(List<Offer> offersList, Context context) {
        super();
        this.offersList = offersList;
        this.context = context;
        Log.d("offer", "StudentFeedAdapter");

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_teacher_feed, parent, false);

        Log.d("offer", "yo");
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //Getting the particular item from the list
        final Offer offer = offersList.get(position);


        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(context, OfferDetailsActivity.class);
                myIntent.putExtra("offer", offer);
                context.startActivity(myIntent);
                ((Activity)context).overridePendingTransition(0, 0);
            }
        });

    }

    @Override
    public int getItemCount() {
        return offersList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public CardView card;

        public ImageView profileImageView;
        public ImageView starOneImageView;
        public ImageView starTwoImageView;
        public ImageView starThreeImageView;
        public ImageView starFourImageView;
        public ImageView starFiveImageView;

        public TextView responseTimeTextView;
        public TextView reliabilityTextView;
        public TextView titleTextView;
        public TextView descriptionTextView;
        public TextView priceTextView;
        public TextView tagsTextView;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);

            Log.d("offer", "ViewHolder");
            card = (CardView) itemView.findViewById(R.id.card);

            profileImageView = (ImageView) itemView.findViewById(R.id.profileImageView);
            starOneImageView = (ImageView) itemView.findViewById(R.id.starOneImageView);
            starTwoImageView = (ImageView) itemView.findViewById(R.id.starTwoImageView);
            starThreeImageView = (ImageView) itemView.findViewById(R.id.starThreeImageView);
            starFourImageView = (ImageView) itemView.findViewById(R.id.starFourImageView);
            starFiveImageView = (ImageView) itemView.findViewById(R.id.starFiveImageView);

            responseTimeTextView = (TextView) itemView.findViewById(R.id.responseTimeTextView);
            reliabilityTextView = (TextView) itemView.findViewById(R.id.reliabilityTextView);
            titleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
            descriptionTextView = (TextView) itemView.findViewById(R.id.descriptionTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.priceTextView);
            tagsTextView = (TextView) itemView.findViewById(R.id.responseTimeTextView);
            responseTimeTextView = (TextView) itemView.findViewById(R.id.tagsTextView);
        }
    }
}