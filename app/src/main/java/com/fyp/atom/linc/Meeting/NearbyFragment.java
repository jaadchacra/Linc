package com.fyp.atom.linc.Meeting;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Login.LoginActivity;
import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Main.StudentFeed.CreateOffer.CreateOfferActivity;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Utils.SQLiteHandler;
import com.fyp.atom.linc.Utils.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class NearbyFragment extends Fragment {

    View view;
    Button startMeetingButton;
    TextView tutorialTextView;
    RatingBar ratingBar;
    String offerID;
    String otherUserID;
    String userName;
    String isTeacher;
    private SessionManager session;
    private SQLiteHandler db;

    public Button teacherBtn;
    public Button studentBtn;
    boolean lastScan = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        offerID = getArguments().getString("offerID");
        otherUserID = getArguments().getString("userID");
        userName = getArguments().getString("userName");
        isTeacher = getArguments().getString("isTeacher");
        Log.d("FragtoAct", "Nearby: "+offerID+" usrid "+otherUserID);

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_nearby, container, false);

        init();

        startMeetingButtonClickListener();

        ratingBarTouchListener();

        return view;
    }

    private void startMeetingButtonClickListener() {
        startMeetingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isTeacher.equals("true"))
                    tutorialTextView.setText("Stop Scanning as soon as "+userName+" Receives your Request so that you preserve Battery.\nYou need to scan again after 30 minutes have elapsed in order to receive your due credits!");
                else{
                    tutorialTextView.setText("After you receive request from "+userName+", a timer will start.\nAfter 30 minutes or more have elapsed, you need to receive another request in order to conclude the meeting!");
                }

                startMeetingButton.setVisibility(View.GONE);

                if(isTeacher.equals("true")) {
                    teacherBtn.setVisibility(View.VISIBLE);
                    teacherBtnClickListener();
                }
                else {
                    studentBtn.setVisibility(View.VISIBLE);
                    studentBtnClickListener();
                }
                startMeetingButton.setVisibility(View.GONE);

//                startMeetingButton.setText("End Meeting");
//                tutorialTextView.setText("Make sure more than 45 minutes have passed and your phones are next to each other when you end meeting, otherwise credits will not be transferred");
            }
        });

    }

    private void teacherBtnClickListener() {
        teacherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MeetingActivity)getActivity()).tutorPressed();


            }
        });
    }

    private void studentBtnClickListener() {
        studentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MeetingActivity)getActivity()).studentPressed();

            }
        });
    }


    private void init() {
        session = new SessionManager(getActivity());

        db = new SQLiteHandler(getActivity());
        HashMap<String, String> hashMap = db.getUserDetails();

        teacherBtn = (Button) view.findViewById(R.id.teacherBtn);
        teacherBtn.setText("Send Request to "+userName);
        teacherBtn.setVisibility(View.GONE);
        studentBtn = (Button) view.findViewById(R.id.studentBtn);
        studentBtn.setText("Scan for "+userName+"'s Request");
        studentBtn.setVisibility(View.GONE);

        ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        ratingBar.setVisibility(View.GONE);

        tutorialTextView = (TextView) view.findViewById(R.id.tutorialTextView);
        tutorialTextView.setText("");

        startMeetingButton = (Button) view.findViewById(R.id.startMeetingButton);
    }


    private void ratingBarTouchListener() {
        ratingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    float touchPositionX = event.getX();
                    float width = ratingBar.getWidth();
                    float starsf = (touchPositionX / width) * 5.0f;
                    final int stars = (int)starsf + 1;
                    ratingBar.setRating(stars);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Submit Rating")
                            .setMessage("Are you sure you want to rate "+userName+" and the course "+stars+"/5?")
                            .setCancelable(true)
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            submitRating(stars);
                            dialog.cancel();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
//                    Toast.makeText(MainActivity.this, String.valueOf("test"), Toast.LENGTH_SHORT).show();
                    v.setPressed(false);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setPressed(true);
                }

                if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    v.setPressed(false);
                }




                return true;
            }});

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void submitRating(final int rating){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://kcapplications.com/Linc/feedback.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        Log.d("responsingz", s);

                        Toast.makeText(getActivity(), "Your feedback has been registered", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                        getActivity().finish();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(getActivity(), "Your feedback was not registered \n Please check your Internet Connection and try again", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("rating", ""+rating);
                params.put("user_id", otherUserID);
                params.put("offer_id", offerID);
                return params;
            }
        };
        MySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

    public void tutorButtonStarted() {
        teacherBtn.setText("Stop Sending Request to "+userName);
    }

    public void tutorButtonStopped() {
        teacherBtn.setText("Send Request to "+userName);
    }

    public void studentButtonStarted() {

        studentBtn.setText("Stop Scanning for "+userName+"'s Request");
    }

    public void studentButtonStopped(boolean turnOffForever) {
        if(lastScan){
            concludeMeeting();
            studentBtn.setVisibility(View.GONE);
            tutorialTextView.setText("Please wait while transfer takes place");
        }else {
            studentBtn.setText("Scan for "+userName+" Request");

            if (turnOffForever) {
                studentBtn.setVisibility(View.GONE);

                new CountDownTimer(1800000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        tutorialTextView.setText("seconds remaining: " + millisUntilFinished / 1000);
                    }

                    public void onFinish() {
                        tutorialTextView.setText("You need to receive another request from "+userName+" in order to conclude the meeting!");
                        studentBtn.setVisibility(View.VISIBLE);
                        lastScan = true;
                    }
                }.start();
            }
        }
    }

    public void clickStudentBtn() {
        studentBtn.callOnClick();
    }

    private void concludeMeeting(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://kcapplications.com/Linc/meetingComplete.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        Log.d("responsingz", s);
                        if(s.equals("success")){
                            ratingBar.setVisibility(View.VISIBLE);
                            tutorialTextView.setText("Please Provide a rating for "+userName+" and the course");
                        }
                        else{
                            concludeMeeting();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        concludeMeeting();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("offerID", offerID);
                return params;
            }
        };
        MySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }


}
