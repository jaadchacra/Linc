package com.fyp.atom.linc.Main.Chats;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fyp.atom.linc.Models.Approval;
import com.fyp.atom.linc.Models.FriendlyMessage;
import com.fyp.atom.linc.Models.User;
import com.fyp.atom.linc.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.fyp.atom.linc.Utils.SQLiteHandler;
import com.fyp.atom.linc.Utils.SessionManager;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class ChatActivity extends AppCompatActivity {

    private static class MessageViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layout1, layout2;
        TextView textView1, textView2;
        public MessageViewHolder(View v) {
            super(v);
            layout1 = (RelativeLayout) itemView.findViewById(R.id.layout1);
            layout2 = (RelativeLayout) itemView.findViewById(R.id.layout2);

            textView1 = (TextView) itemView.findViewById(R.id.textView1);
            textView2 = (TextView) itemView.findViewById(R.id.textView2);
        }
    }

    public String MESSAGES_CHILD;
    private String mUsername;
    Approval approval;
    User otherUser;
    private ImageView mSendButton;
    private RecyclerView mMessageRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressBar mProgressBar;
    private EditText mMessageEditText;
    private SQLiteHandler db;


    // Firebase instance variables
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseRecyclerAdapter<FriendlyMessage, MessageViewHolder> mFirebaseAdapter;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(0, 0);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        init();

        setToolbar();

        initFirebaseDatabase();

        initFirebaseAdapter();

        messageEditTextChangeListener();

        sendButtonClickListener();

    }

    public void setToolbar(){
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar_layout);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#00936D"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);

        TextView actionBarTitle = (TextView) findViewById(R.id.action_bar_title);
        actionBarTitle.setText(otherUser.getName());
    }

    private void init() {
        Intent mIntent = getIntent();
        approval = (Approval) mIntent.getSerializableExtra("Approval");
        otherUser = approval.getUser();


        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        HashMap<String, String> hashMap = db.getUserDetails();
        //Looping through hashmap to see values
//        for (String key : hashMap.keySet()) {
//            Log.d("hahahsgsgs","" + key);
//            Log.d("hahahsgsgs","" + hashMap.get(key));
//        }
        mUsername = hashMap.get("name");
        int myID = Integer.parseInt(hashMap.get("uid"));
        int otherID = otherUser.getUserID();
        if(myID < otherID)
            MESSAGES_CHILD =  myID+ "-" +otherID ;
        else
            MESSAGES_CHILD =  otherID+ "-" +myID ;

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        //Initialize RecyclerView
        mMessageRecyclerView = (RecyclerView) findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        //Stacks from bottom of the list
        mLinearLayoutManager.setStackFromEnd(true);
        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);

        mMessageEditText = (EditText) findViewById(R.id.messageEditText);

        mSendButton = (ImageView) findViewById(R.id.sendButton);
    }

    private void initFirebaseDatabase() {
        // New child entries
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
    }

    private void initFirebaseAdapter() {
        mFirebaseAdapter = new FirebaseRecyclerAdapter<FriendlyMessage, MessageViewHolder>(FriendlyMessage.class,
                R.layout.list_item_message, MessageViewHolder.class, mFirebaseDatabaseReference.child(MESSAGES_CHILD)) {
            @Override
            protected void populateViewHolder(final MessageViewHolder viewHolder, FriendlyMessage friendlyMessage, int position) {
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
                if(friendlyMessage.getName().equals(mUsername)){
                    viewHolder.layout1.setVisibility(View.VISIBLE);
                    viewHolder.layout2.setVisibility(View.GONE);
                    viewHolder.textView1.setText(friendlyMessage.getText());
                } else{
                    viewHolder.layout2.setVisibility(View.VISIBLE);
                    viewHolder.layout1.setVisibility(View.GONE);
                    viewHolder.textView2.setText(friendlyMessage.getText());
                }
            }
        };

        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the
                // user is at the bottom of the list, scroll to the bottom
                // of the list to show the newly added message.
                if (lastVisiblePosition == -1 || (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
                    mMessageRecyclerView.scrollToPosition(positionStart);
                }
            }
        });

        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
        mMessageRecyclerView.setAdapter(mFirebaseAdapter);
    }

    private void messageEditTextChangeListener() {
        mMessageEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1000)});
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void sendButtonClickListener() {
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Send messages on click.
                FriendlyMessage friendlyMessage = new
                        FriendlyMessage(mMessageEditText.getText().toString(),
                        mUsername,
                        null,
                        null /* no image */);
                mFirebaseDatabaseReference.child(MESSAGES_CHILD).push().setValue(friendlyMessage);
                mMessageEditText.setText("");
            }
        });
    }
}
