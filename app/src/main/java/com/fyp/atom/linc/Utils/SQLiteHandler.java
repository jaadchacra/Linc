package com.fyp.atom.linc.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class SQLiteHandler extends SQLiteOpenHelper {

    private SQLiteDatabase sqLiteDatabase;

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "android_api";

    // Login table name
    private static final String TABLE_USER = "user";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_UID = "uid";
    private static final String KEY_CREATED_AT = "created_at";
    private static final String KEY_CREDIT = "credit";

    private static final String KEY_RESPONSE = "userResponse";
    private static final String KEY_REPUTATION = "userReputation";
    private static final String KEY_RATING = "userRating";
    private static final String KEY_FACEBOOK = "userFacebook";
    private static final String KEY_INSTAGRAM = "userInstagram";
    private static final String KEY_TWITTER = "userTwitter";


    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE," + KEY_UID + " TEXT,"
                + KEY_CREATED_AT + " TEXT," + KEY_CREDIT + " INTEGER,"
                + KEY_RESPONSE + " INTEGER,"
                + KEY_REPUTATION + " INTEGER,"
                + KEY_RATING + " INTEGER,"
                + KEY_FACEBOOK + " TEXT,"
                + KEY_INSTAGRAM + " TEXT,"
                + KEY_TWITTER + " TEXT" + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addUser(String name, String email, String uid, String created_at, String credit, String userResponse, String userReputation, String userRating, String userFacebook, String userInstagram, String userTwitter) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name); // Name
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_UID, uid); // Email
        values.put(KEY_CREATED_AT, created_at); // Created At
        values.put(KEY_CREDIT, credit);

        values.put(KEY_RESPONSE, userResponse);
        values.put(KEY_REPUTATION, userReputation);
        values.put(KEY_RATING, userRating);
        values.put(KEY_FACEBOOK, userFacebook);
        values.put(KEY_INSTAGRAM, userInstagram);
        values.put(KEY_TWITTER, userTwitter);


        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("name", cursor.getString(1));
            user.put("email", cursor.getString(2));
            user.put("uid", cursor.getString(3));
            user.put("created_at", cursor.getString(4));
            user.put("credit", cursor.getString(5));
            user.put("userResponse", cursor.getString(6));
            user.put("userReputation", cursor.getString(7));
            user.put("userRating", cursor.getString(8));
            user.put("userFacebook", cursor.getString(9));
            user.put("userInstagram", cursor.getString(10));
            user.put("userTwitter", cursor.getString(11));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    /**
     * Edits the credits by adding or subtracting depending on boolean value
     * */
    public void editCredits(int currentCredit, int newCredit, boolean isAddCredit){
        int myNewCredit = currentCredit;
        if(isAddCredit) {
            myNewCredit += newCredit;
        } else{
            myNewCredit -= newCredit;
        }

        ContentValues values = new ContentValues();
        values.put(KEY_CREDIT, myNewCredit);
//        sqLiteDatabase.update(TABLE_USER, values, null, null);
        String updateQuery = "UPDATE " + TABLE_USER + " SET "+ KEY_CREDIT +" = "+ myNewCredit;
        SQLiteDatabase db= this.getWritableDatabase();
        db.execSQL(updateQuery);

    }

    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }

}
