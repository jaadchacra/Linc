package com.fyp.atom.linc.Main.Categories;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fyp.atom.linc.Main.Categories.Courses.CoursesActivity;
import com.fyp.atom.linc.Main.Categories.Recommender.RecommenderSpecialActivity;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Models.Category;

import java.util.List;


public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private Context context;

//    ImageButton retryButtonDiscount = null;

    //List to store all superheroes
    List<Category> categoryList;

    //Constructor of this class
    public CategoriesAdapter(List<Category> categoryList, Context context) {
        super();
        //Getting all superheroes
        this.categoryList = categoryList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_categories, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        //Getting the particular item from the list
        final Category category = categoryList.get(position);

        String uri = "@drawable/" + category.getDrawableName();  // where myresource (without the extension) is the file
        int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
        Drawable res = context.getResources().getDrawable(imageResource);
        holder.imageViewCategory.setImageDrawable(res);


        holder.imageViewCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("categoryNamezzz", category.getCategoryName());
//                if(category.getCategoryName().equals("Random")){
//                    Intent myIntent = new Intent(context, RecommenderSpecialActivity.class);
////
//                    myIntent.putExtra("categoryName", category.getCategoryName());
////                myIntent.putExtra("logo", logo);
////
//                    context.startActivity(myIntent);
//                    ((Activity)context).overridePendingTransition(0, 0);
//                }else{
                    //TODO: make intent to activity or reload a new fragment??
                    //TODO: get activity instead of context so it doesn't open second instance of app? or is this only because of my phone? :s, test on other phone
                    Intent myIntent = new Intent(context, CoursesActivity.class);
//
                    myIntent.putExtra("categoryName", category.getCategoryName());
//                myIntent.putExtra("logo", logo);
//
                    context.startActivity(myIntent);
                    ((Activity)context).overridePendingTransition(0, 0);
//                }




            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public ImageView imageViewCategory;


        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            imageViewCategory = (ImageView) itemView.findViewById(R.id.imageViewCategory);

        }
    }
}