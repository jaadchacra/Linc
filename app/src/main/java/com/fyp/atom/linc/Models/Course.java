package com.fyp.atom.linc.Models;

import java.io.Serializable;

/**
 * Created by ATOM on 11/26/2016.
 */

public class Course implements Serializable {
    public int getCourseID() {
        return courseID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    int courseID;
    String Name;
    String Category_Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }


    public String getCategory_Name() {
        return Category_Name;
    }

    public void setCategory_Name(String category_Name) {
        Category_Name = category_Name;
    }

}

