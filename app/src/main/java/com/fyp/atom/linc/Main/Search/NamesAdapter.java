package com.fyp.atom.linc.Main.Search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.fyp.atom.linc.Models.Name;
import com.fyp.atom.linc.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ATOM on 11/18/2016.
 */
//Adapter for autocomplete
public class NamesAdapter extends ArrayAdapter<Name> {

    Context context;
    int resource, textViewResourceId;
    List<Name> items, tempItems, suggestions;

    public NamesAdapter(Context context, int resource, int textViewResourceId, List<Name> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<Name>(items); // this makes the difference.
        suggestions = new ArrayList<Name>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.autocomplete_item, parent, false);
        }
        Name name = items.get(position);
        if (name != null) {
            TextView lblName = (TextView) view.findViewById(R.id.lbl_name);
            if (lblName != null)
                lblName.setText(name.name);
        }
        return view;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    /**
     * Custom Filter implementation for custom suggestions we provide.
     */
    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((Name) resultValue).name;
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Name name : tempItems) {
                    if (name.name.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(name);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<Name> filterList = (ArrayList<Name>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (Name name : filterList) {
                    add(name);
                    notifyDataSetChanged();
                }
            }
        }
    };
}