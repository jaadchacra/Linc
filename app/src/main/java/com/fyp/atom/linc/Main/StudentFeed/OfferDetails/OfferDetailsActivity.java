package com.fyp.atom.linc.Main.StudentFeed.OfferDetails;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.fyp.atom.linc.Main.StudentFeed.OfferDetails.SessionSelection.SessionSelectionActivity;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.R;
import com.squareup.picasso.Picasso;

public class OfferDetailsActivity extends AppCompatActivity {
    ImageView starOneImageView, starTwoImageView, starThreeImageView, starFourImageView, starFiveImageView, offerImageView;
    Toolbar offerDetailsToolbar;
    TextView nameTextView, titleTextView, toolbarTextView, descriptionTextView, priceTextView, tagsTextView, timeSlotTextView, responseTimeTextView, reliabilityTextView;
    String courseName = "";
    String categoryName = "";
    Button continueButton;
    Offer offer;
    ImageView profileImageView;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(0, 0);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);

        init();

        continueButtonClickListener();

        String url = "http://kcapplications.com/Linc/images/offer"+offer.getOfferID()+".jpeg";
        Picasso.with(this)
                .load(url)
                .into(offerImageView);

        String logoUrl = "http://kcapplications.com/Linc/images/" + offer.getUser().getUserEmail() +"_logo.jpg";
        Log.d("logoooo", logoUrl);
        Picasso.with(this)
                .load(logoUrl)
                .placeholder(R.drawable.placeholder)
                .into(profileImageView);

    }

    private void continueButtonClickListener() {
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getBaseContext(), SessionSelectionActivity.class);
                myIntent.putExtra("offer", offer);
                startActivity(myIntent);
                overridePendingTransition(0, 0);
            }
        });
    }

    private void init() {
        Intent mIntent = getIntent();
        offer = (Offer) mIntent.getSerializableExtra("offer");
        courseName = mIntent.getExtras().getString("courseName");

        profileImageView = (ImageView) findViewById(R.id.profileImageView);
        offerImageView = (ImageView) findViewById(R.id.offerImageView);
        starOneImageView = (ImageView) findViewById(R.id.starOneImageView);
        starTwoImageView = (ImageView) findViewById(R.id.starTwoImageView);
        starThreeImageView = (ImageView) findViewById(R.id.starThreeImageView);
        starFourImageView = (ImageView) findViewById(R.id.starFourImageView);
        starFiveImageView = (ImageView) findViewById(R.id.starFiveImageView);

        nameTextView = (TextView) findViewById(R.id.nameTextView);
        titleTextView = (TextView) findViewById(R.id.titleTextView);
        toolbarTextView = (TextView) findViewById(R.id.toolbarTextView);
        descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
        priceTextView = (TextView) findViewById(R.id.priceTextView);
        timeSlotTextView = (TextView) findViewById(R.id.timeSlotTextView);
        tagsTextView = (TextView) findViewById(R.id.tagsTextView);
        offerDetailsToolbar = (Toolbar) findViewById(R.id.offerDetailsToolbar);
        responseTimeTextView = (TextView) findViewById(R.id.responseTimeTextView);

        reliabilityTextView = (TextView) findViewById(R.id.reliabilityTextView);

        continueButton = (Button) findViewById(R.id.continueButton);

        setRatingImage(offer);
        timeSlotTextView.setText(offer.getDateStr());
        setTags(offer);
        priceTextView.setText("" + offer.getPrice());
        responseTimeTextView.setText("Response Time: " + offer.getUser().getUserResponse() + "h");
        reliabilityTextView.setText("Reliability: " + offer.getUser().getUserReliability() + "%");
        nameTextView.setText(offer.getUser().getName());
        titleTextView.setText(offer.getTitle());
        descriptionTextView.setText(offer.getDescription());

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular_0.ttf");
        toolbarTextView.setTypeface(tf);
        toolbarTextView.setText(offer.getCourse().getName());
        categoryName = offer.getCategory().getCategoryName();
        String categoryBarImgName = categoryName;
        categoryBarImgName = categoryBarImgName.toLowerCase() + "_bar";
        int resId = getResources().getIdentifier(categoryBarImgName, "drawable", getPackageName());
        setSupportActionBar(offerDetailsToolbar);
        offerDetailsToolbar.setBackgroundResource(resId);
        setSupportActionBar(offerDetailsToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setRatingImage(Offer offer) {
        String uriStarOn = "@drawable/star_on";  // where myresource (without the extension) is the file
        int imageResourceOn = getResources().getIdentifier(uriStarOn, null, getPackageName());
        Drawable starOn = getResources().getDrawable(imageResourceOn);

        String uriStarOff = "@drawable/star_off";  // where myresource (without the extension) is the file
        int imageResourceOff = getResources().getIdentifier(uriStarOff, null, getPackageName());
        Drawable starOff = getResources().getDrawable(imageResourceOff);

        if (offer.getUser().getUserRating() == 0) {
            starOneImageView.setImageDrawable(starOff);
            starTwoImageView.setImageDrawable(starOff);
            starThreeImageView.setImageDrawable(starOff);
            starFourImageView.setImageDrawable(starOff);
            starFiveImageView.setImageDrawable(starOff);
        } else if (offer.getUser().getUserRating() == 1) {
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOff);
            starThreeImageView.setImageDrawable(starOff);
            starFourImageView.setImageDrawable(starOff);
            starFiveImageView.setImageDrawable(starOff);
        } else if (offer.getUser().getUserRating() == 2) {
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOn);
            starThreeImageView.setImageDrawable(starOff);
            starFourImageView.setImageDrawable(starOff);
            starFiveImageView.setImageDrawable(starOff);
        } else if (offer.getUser().getUserRating() == 3) {
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOn);
            starThreeImageView.setImageDrawable(starOn);
            starFourImageView.setImageDrawable(starOff);
            starFiveImageView.setImageDrawable(starOff);
        } else if (offer.getUser().getUserRating() == 4) {
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOn);
            starThreeImageView.setImageDrawable(starOn);
            starFourImageView.setImageDrawable(starOn);
            starFiveImageView.setImageDrawable(starOff);
        } else if (offer.getUser().getUserRating() == 4) {
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOn);
            starThreeImageView.setImageDrawable(starOn);
            starFourImageView.setImageDrawable(starOn);
            starFiveImageView.setImageDrawable(starOn);
        }
    }

    public void setTags(Offer offer) {
        String tags = "#" + offer.getTagsList().get(0).getName();
        for (int i = 1; i < offer.getTagsList().size(); i++) {
            tags = tags + " #" + offer.getTagsList().get(i).getName();
        }
        tagsTextView.setText(tags);
    }


}
