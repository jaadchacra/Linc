package com.fyp.atom.linc.Registration.TeachingInterests;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Registration.LearningInterestsActivity.LearningInterestsActivity;
import com.fyp.atom.linc.Utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

//import org.json.JSONObject;

public class TeachingInterestsActivity extends AppCompatActivity {

    private String UPLOAD_URL = "http://kcapplications.com/Linc/postCourses.php";
    //    EditText etTextIn;
    EditText etPhoneNumber;
    ArrayList<String> courses;
    ArrayList<String> prices;

    //    EditText etWebsite;
//    EditText etFacebook;
//    EditText etTwitter;
//    EditText etInstagram;
    //status of shop (stupid idea we didn't go with)
    EditText etStatus;
    //contains the branches that will be added dynamically
    LinearLayout llcontainer;
    Button bAdd;
    Button bsubmit;
    List<TextView> AllEts;
    List<TextView> AllPhoneEts;
    String website, facebook, twitter, instagram, branches, phoneNumbers;
    ArrayList<String> arrayList;
    SharedPreferences prefs;
    Spinner spinner;
    Spinner courseSpinner;
    //course_spin
    Bundle globalSavedInstanceState;
    String submitCourses = "";
    String submitPrices = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teaching_interests);

        init();

        //Function to remove keyboard on touch of screen if you didn't touch an editTextView
        setupUI(findViewById(R.id.linear_layout_parent));

        spinnerListener();

        // Volley request to fill the items
        getData(globalSavedInstanceState, "http://kcapplications.com/Linc/getCategories.php", "Categories");
        getData(globalSavedInstanceState, "http://kcapplications.com/Linc/courses.php?category=Sports", "Courses");

        addButtonClickListener();

        submitButtonClickListener();
    }

    private void submitButtonClickListener() {
        //On Submit
        bsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Check if all fields are filled
                //TODO: Crashes if course not added from spinner
                submitCourses = "" + courses.get(0).replaceAll(",", "!");
                for (int i = 1; i < courses.size(); i++) {
                    //Log.d("test",AllEts.get(i).getText().toString());
                    submitCourses = submitCourses + ", " + courses.get(i).toString().replaceAll(",", "!");
                }

                submitPrices = "" + prices.get(0).toString().replaceAll(",", "!");
                for (int i = 1; i < prices.size(); i++) {
                    //Log.d("test",AllEts.get(i).getText().toString());
                    submitPrices = submitPrices + ", " + prices.get(i).toString().replaceAll(",", "!");
                }

                Log.d("testing1", submitCourses);
                Log.d("testing2", submitPrices);

                final ProgressDialog loading = ProgressDialog.show(TeachingInterestsActivity.this, "Submitting info...", "Please wait...", false, false);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String s) {
                                //Disimissing the progress dialog
                                loading.dismiss();
                                Intent myIntent = new Intent(TeachingInterestsActivity.this, LearningInterestsActivity.class);
                                TeachingInterestsActivity.this.startActivity(myIntent);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                if (volleyError instanceof NoConnectionError) {
                                    loading.dismiss();
                                    showAlertDialog(TeachingInterestsActivity.this, "No Internet connection", "You don't have internet connection");
                                } else if (volleyError instanceof NetworkError) {
                                    loading.dismiss();
                                    showAlertDialog(TeachingInterestsActivity.this, "No Internet connection", "You don't have internet connection");
                                } else if (volleyError instanceof TimeoutError) {
                                    loading.dismiss();
                                    showAlertDialog(TeachingInterestsActivity.this, "Connection error", "Failed to connect to server");
                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new Hashtable<String, String>();
                        //Adding parameters
                        String email = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("email", "");
                        params.put("email", email);
                        params.put("courses", submitCourses);
                        params.put("prices", submitPrices);
                        return params;
                    }
                };
                //Creating a Request Queue
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                //Adding request to the queue
                requestQueue.add(stringRequest);
//                }
            }
        });
    }

    private void addButtonClickListener() {
        // If we came to this activity from upload Discounts, it means they already have entered info, so we need to load it for them
        bAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (etPhoneNumber.getText().toString().replace(" ", "").length() == 0) {
                    if (etPhoneNumber.getText().toString().replace(" ", "").length() == 0)
                        etPhoneNumber.setError("Please enter the branch phone number");
                } else {
                    LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.activity_info_row, null);
                    final TextView textOut = (TextView) addView.findViewById(R.id.textout);
                    final TextView textPhoneOut = (TextView) addView.findViewById(R.id.text_phone_out);

                    prices.add(etPhoneNumber.getText().toString());
                    courses.add(courseSpinner.getSelectedItem().toString());

                    textPhoneOut.setText(courseSpinner.getSelectedItem() + " with price: " + etPhoneNumber.getText().toString() + " credits");
                    etPhoneNumber.setText("");
                    AllPhoneEts.add(textPhoneOut);
                    etPhoneNumber.setError(null);

                    Button buttonRemove = (Button) addView.findViewById(R.id.remove);
                    buttonRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((LinearLayout) addView.getParent()).removeView(addView);
                            AllEts.remove(textOut);
                            AllPhoneEts.remove(textPhoneOut);
                        }
                    });
                    llcontainer.addView(addView);
                }
            }
        });
    }

    private void spinnerListener() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                getData(globalSavedInstanceState, "http://kcapplications.com/Linc/courses.php?category=" + spinner.getSelectedItem().toString(), "Courses");
                if (position != 0) {
                    courseSpinner.setEnabled(true);
                } else {
                    courseSpinner.setEnabled(false);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void init() {
        spinner = (Spinner) findViewById(R.id.my_spin);
        courseSpinner = (Spinner) findViewById(R.id.course_spin);
        courseSpinner.setEnabled(false);
        spinner.setPrompt("Choose Category");
        courseSpinner.setPrompt("Choose Subject");

        courses = new ArrayList<>();
        prices = new ArrayList<>();
        bAdd = (Button) findViewById(R.id.add);
        llcontainer = (LinearLayout) findViewById(R.id.container);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        bsubmit = (Button) findViewById(R.id.bSubmit);

        AllEts = new ArrayList<>();
        AllPhoneEts = new ArrayList<>();
        prefs = this.getSharedPreferences("kfucha.insaleshops", Context.MODE_PRIVATE);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar_layout);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#00936D"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        TextView actionBarTitle = (TextView) findViewById(R.id.action_bar_title);
        actionBarTitle.setText("User Info");
    }

    public void showAlertDialog(Context context, String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                TeachingInterestsActivity.this);
        builder.setTitle(title).setMessage(msg).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException err) {

        }
    }

    private void getData(final Bundle savedInstanceState, String url, String listName) {
        MySingleton.getInstance(this).addToRequestQueue(getDataFromServer(savedInstanceState, url, listName));
    }

    private JsonArrayRequest getDataFromServer(final Bundle savedInstanceState, final String url, final String listName) {
        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if (response != null) {
                            parseData(response, listName);
                        } else {
                            getData(globalSavedInstanceState, url, listName);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getData(globalSavedInstanceState, url, listName);
                    }
                });
        return jsonArrayRequest;
    }

    //This method will parse json data
    private void parseData(JSONArray array, String listName) {
        ArrayList<String> list = new ArrayList<String>();

        if (listName == "Courses") {
            list.add(0, "Choose Subject");
        } else {
            list.add(0, "Choose Category");
        }

        for (int i = 0; i < array.length(); i++) {
            String course = "";
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);
//                //Getting the course
                if (listName == "Courses") {
                    course = json.getString("courseName");
                    Log.d("testAdapter", "Courses"+course);
                } else {
                    course = json.getString("categoryName");
                    Log.d("testAdapter", "Category"+course);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            list.add(i + 1, course);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
            if (listName == "Courses") {
                courseSpinner.setAdapter(adapter);
            } else {
                spinner.setAdapter(adapter);
            }
        }
//        //Notifying the adapter that data has been added or changed
    }


    public void setupUI(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(TeachingInterestsActivity.this);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public int getPx(int dimensionDp) {
        float density = getResources().getDisplayMetrics().density;
        return (int) (dimensionDp * density + 0.5f);
    }
}
