package com.fyp.atom.linc.Main;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Main.Chats.ChatsFragment;
import com.fyp.atom.linc.Main.Profile.ProfileFragment;
import com.fyp.atom.linc.Main.Categories.CategoriesFragment;
import com.fyp.atom.linc.Main.Search.SearchActivity;
import com.fyp.atom.linc.Main.Sessions.SessionsFragment;
import com.fyp.atom.linc.Main.StudentFeed.StudentFeedFragment;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Registration.ProfileImage.ImageViewActivity;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Utils.SQLiteHandler;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ViewPager viewPager;
    private BottomBar mBottomBar;
    RelativeLayout relProfileToolbar, relTextToolbar;
    TextView toolbarTextView, changingTextView, availableCreditTextView;
    private AutoCompleteTextView autoCompleteTextView;
    private SQLiteHandler db;
    ProfileFragment profileFragment;
    SessionsFragment sessionsFragment;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //TODO: see what this does, I removed when I added George Login
//        Intent mIntent = getIntent();
//        isFailedSplash =  mIntent.getExtras().getBoolean("isFailed");
        Log.d("ActivityNow", "I'm in MainActivity!");

        relProfileToolbar = (RelativeLayout) findViewById(R.id.relProfileToolbar);
        relTextToolbar = (RelativeLayout) findViewById(R.id.relTextToolbar);
        toolbarTextView = (TextView) findViewById(R.id.toolbarTextView);
//        availableCreditTextView = (TextView) findViewById(R.id.availableCreditTextView);
        changingTextView = (TextView) findViewById(R.id.changingTextView);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular_0.ttf");
        toolbarTextView.setTypeface(tf);
        changingTextView.setTypeface(tf);
        toolbar = (Toolbar) findViewById(R.id.toolbar_courses);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setVisibility(View.GONE);

        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autocompleteView);
        //Since we're starting on stores fragment
        autoCompleteTextView.setVisibility(View.VISIBLE);

        autoCompleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getBaseContext(), SearchActivity.class);
                startActivity(myIntent);
                overridePendingTransition(0, 0);
            }
        });


        // SQLite database handler
        db = new SQLiteHandler(this);
        HashMap<String, String> hashMap = db.getUserDetails();
        userID = hashMap.get("uid");

//        db.editCredits(Integer.parseInt(hashMap.get("credit")), 73, true);
//        int availableCredit = Integer.parseInt(hashMap.get("credit"));
//        availableCreditTextView.setText(""+availableCredit);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        mBottomBar = BottomBar.attachShy((CoordinatorLayout) findViewById(R.id.myCoordinator_courses),
                findViewById(R.id.viewpager), savedInstanceState);
        mBottomBar.setItems(R.menu.bottombar_menu);

        mBottomBar.setOnMenuTabClickListener(new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBarItemOne) {
                    viewPager.setCurrentItem(0, true);// The user selected item number one.
                    autoCompleteTextView.setVisibility(View.VISIBLE);
                    relProfileToolbar.setVisibility(View.GONE);
                    relTextToolbar.setVisibility(View.GONE);
                }
                if (menuItemId == R.id.bottomBarItemTwo) {
                    viewPager.setCurrentItem(1, true);// The user selected item number two.
                    autoCompleteTextView.setVisibility(View.GONE);
                    relTextToolbar.setVisibility(View.GONE);
                    relProfileToolbar.setVisibility(View.VISIBLE);
                    changingTextView.setText("This Week's Offers");
                }
//
                if (menuItemId == R.id.bottomBarItemThree) {
                    viewPager.setCurrentItem(2, true);// The user selected item number three.
                    autoCompleteTextView.setVisibility(View.GONE);
                    relTextToolbar.setVisibility(View.GONE);
                    relProfileToolbar.setVisibility(View.VISIBLE);
                    changingTextView.setText("My WishLists");
                }
                if (menuItemId == R.id.bottomBarItemFour) {
                    viewPager.setCurrentItem(3, true);// The user selected item number four.
                    autoCompleteTextView.setVisibility(View.GONE);
                    relTextToolbar.setVisibility(View.VISIBLE);
                    relProfileToolbar.setVisibility(View.GONE);
                    toolbarTextView.setText("Chat for Upcoming Meetings");
                }
                if (menuItemId == R.id.bottomBarItemFive) {
                    viewPager.setCurrentItem(4, true);// The user selected item number five.
                    autoCompleteTextView.setVisibility(View.GONE);
                    relTextToolbar.setVisibility(View.VISIBLE);
                    relProfileToolbar.setVisibility(View.GONE);
                    toolbarTextView.setText("My Profile");
                    getUserCredits();
                }
            }



            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.bottomBarItemOne) {
                    // The user reselected item number one, scroll your content to top.
                }
                if (menuItemId == R.id.bottomBarItemTwo) {
                    // The user reselected item number two, scroll your content to top.
                }
                if (menuItemId == R.id.bottomBarItemThree) {
                    // The user reselected item number three, scroll your content to top.
                }
                if (menuItemId == R.id.bottomBarItemFour) {
                    // The user reselected item number four, scroll your content to top.
                }
                if (menuItemId == R.id.bottomBarItemFive) {
                    // The user reselected item number four, scroll your content to top.
                }
            }
        });

         /*
        *
        * THESE COLORS WILL ONLY BE TAKEN IF YOU HAVE MORE THAN
        * 3 items in res -> Menu -> bottombarbullshit
        * Otherwise Grey Bar
        *
        * */
        // Setting colors for different tabs when there's more than three of them.
        // You can set colors for tabs in three different ways as shown below.
        mBottomBar.mapColorForTab(0, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(1, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(2, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(3, ContextCompat.getColor(this, R.color.colorPrimary));
        mBottomBar.mapColorForTab(4, ContextCompat.getColor(this, R.color.colorPrimary));
    }

    /*
    * View Pager adapter for the Tabs of SALES and STORES
    * */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        profileFragment = new ProfileFragment();
        sessionsFragment = new SessionsFragment();
        adapter.addFragment(new CategoriesFragment(), "Categories");
        adapter.addFragment(new StudentFeedFragment(), "SFeed");
        adapter.addFragment(new SessionsFragment(), "Sessions");
        adapter.addFragment(new ChatsFragment(), "Chats");
        adapter.addFragment( profileFragment , "Profile");



        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        Runtime.getRuntime().gc();
//    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Necessary to restore the BottomBar's state, otherwise we would
        // lose the current tab on orientation change.
        mBottomBar.onSaveInstanceState(outState);
    }



    private void getUserCredits() {
        MySingleton.getInstance(this).addToRequestQueue(getSessionsFromServer());
    }
    private StringRequest getSessionsFromServer() {
        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, "http://kcapplications.com/Linc/user/myCredits.php?user_id="+userID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("sessions", response.toString());
                        if (response != null) {
                            profileFragment.setCredits(response);
                        } else {
                            getUserCredits();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {


                                getUserCredits();

                    }
                });
        return postRequest;
    }
}
