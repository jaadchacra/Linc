package com.fyp.atom.linc.Login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Meeting.MeetingActivity;
import com.fyp.atom.linc.Models.Category;
import com.fyp.atom.linc.Models.Course;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.Models.Tag;
import com.fyp.atom.linc.Models.User;
import com.fyp.atom.linc.Registration.ProfileImage.ImageViewActivity;
import com.fyp.atom.linc.Registration.RegisterActivity;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Registration.SocialMedia.SocialMediaActivity;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.app.AppConfig;
import com.fyp.atom.linc.app.AppController;
import com.fyp.atom.linc.Utils.SQLiteHandler;
import com.fyp.atom.linc.Utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends Activity {
    Bundle mSavedInstanceState;
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnLogin;
    private Button btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;
    SharedPreferences prefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mSavedInstanceState = savedInstanceState;

        init();

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());
        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            pDialog.setMessage("Logging in ...");
            showDialog();

            try {
                checkIfInMeeting(mSavedInstanceState);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("email", email).commit();

                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {
                    // login user
                    checkLogin(email, password);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        // Link to Register Screen
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    private void init() {
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
    }

    /**
     * function to verify login details in mysql db
     * */
    private void checkLogin(final String email, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite

                        JSONObject user = jObj.getJSONObject("User");
                        String uid = user.getString("userID");
                        String name = user.getString("userFirstName");
                        String email = user.getString("userEmail");
                        String created_at = user.getString("userCreatedAt");

                        String credit= user.getString("userAvailableCredits");
                        String userResponse = user.getString("userResponse");
                        String userReputation = user.getString("userReputation");
                        String userRating = user.getString("userRating");
                        String userFacebook = user.getString("userFacebook");
                        String userInstagram = user.getString("userInstagram");
                        String userTwitter = user.getString("userTwitter");

                        // Inserting row in users table
                        db.addUser(name, email, uid, created_at, credit,userResponse,userReputation,userRating,userFacebook,userInstagram,userTwitter);

                        // user registered just now
                        if(userFacebook.equals("") && userInstagram.equals("") && userTwitter.equals("")){
                            hideDialog();
                            Intent intent = new Intent(LoginActivity.this, SocialMediaActivity.class);
                            startActivity(intent);
                            finish();
                        } else{

                            checkIfInMeeting(mSavedInstanceState);


                        }


                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                        hideDialog();
                    }
                } catch (JSONException e) {
                    // JSON error
                    hideDialog();
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "test error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    hideDialog();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void checkIfInMeeting(final Bundle savedInstanceState) throws UnsupportedEncodingException {
        MySingleton.getInstance(this).addToRequestQueue(checkIfInMeetingFromServer(savedInstanceState));
    }
    private StringRequest checkIfInMeetingFromServer(final Bundle savedInstanceState) throws UnsupportedEncodingException {
        //Displaying Progressbar
        HashMap<String, String> hashMap = db.getUserDetails();
        String myID = hashMap.get("uid");
        String myDate = URLEncoder.encode(dateStrNow(), "utf-8");;
        String url = "http://kcapplications.com/Linc/meeting.php?" + "date="+myDate+"&user_id="+myID;

        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideDialog();

                        Log.d("offerActivityjadofferrs", response.toString());
                        if (response.equals("{\"offerID\":null,\"isInMeeting\":false}")){
                            // Launch main activity
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            //extra
                            startActivity(intent);
                            finish();
                        } else{
                            JSONObject jsonObj = null;
                            try {
                                jsonObj = new JSONObject(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String offerID = "";
                            String userID = "";
                            String userName = "";
                            String isTeacher = "";

                            try {
                                offerID = jsonObj.getString("offerID");
                                userID = jsonObj.getString("userID");
                                userName = jsonObj.getString("userFirstName");
                                isTeacher = jsonObj.getString("teacher");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Intent intent = new Intent(LoginActivity.this, MeetingActivity.class);
                            intent.putExtra("offerID", offerID);
                            intent.putExtra("userID", userID);
                            intent.putExtra("userName", userName);
                            intent.putExtra("isTeacher", isTeacher);

                            startActivity(intent);
                            finish();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
                        Toast.makeText(LoginActivity.this, "Please Check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                });
        return postRequest;
    }
    private String dateStrNow() {
        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowStr = sdf.format(now.getTime());
        return nowStr;
    }
}
