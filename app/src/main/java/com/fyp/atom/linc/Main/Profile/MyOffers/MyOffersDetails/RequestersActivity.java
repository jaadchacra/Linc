package com.fyp.atom.linc.Main.Profile.MyOffers.MyOffersDetails;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Main.StudentFeed.OfferDetails.SessionSelection.SessionSelectionActivity;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.Models.Requester;
import com.fyp.atom.linc.Models.Session;
import com.fyp.atom.linc.Models.User;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class RequestersActivity extends AppCompatActivity {
    Bundle mSavedInstanceState;
    Toolbar sessionDetailsToolbar;
    TextView toolbarTextView;
    private List<Requester> requesterssList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    Offer offer;
    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(0, 0);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requesters);

        init();

        getRequesters(mSavedInstanceState);

    }


    private void init() {
        Intent mIntent = getIntent();
        offer = (Offer) mIntent.getSerializableExtra("offer");
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        progressBarCenter = (ProgressBar) findViewById(R.id.progressBarCenter);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) findViewById(R.id.errorMessageTextView);
        retryButton = (Button) findViewById(R.id.retryButton);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        toolbarTextView = (TextView) findViewById(R.id.toolbarTextView);
        sessionDetailsToolbar = (Toolbar) findViewById(R.id.sessionDetailsToolbar);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular_0.ttf");
        toolbarTextView.setTypeface(tf);
        toolbarTextView.setText(offer.getTitle());
        setSupportActionBar(sessionDetailsToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setSupportActionBar(sessionDetailsToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        requesterssList = new ArrayList<>();


    }

    private void getRequesters(Bundle mSavedInstanceState) {
        MySingleton.getInstance(this).addToRequestQueue(getRequestersFromServer(mSavedInstanceState));
    }


    private StringRequest getRequestersFromServer(final Bundle savedInstanceState) {
        //Displaying Progressbar
        progressBarCenter.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, " http://kcapplications.com/Linc/users.php?offer_id=" + offer.getOfferID(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("offerrs", response.toString());
                        if (response != null) {
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getRequesters(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("offerrs", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getRequesters(savedInstanceState);
                            }
                        });
                    }
                });
        return postRequest;
    }

    //This method will parse json data
    private void parseData(String response) {

        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                Requester requester = new Requester();
                User user = new User();
                requester.setRequester(user);

                JSONObject userJsonObject = null;

                try {
                    Log.d("jadofferrs", "in parse");
                    Log.d("jadofferrs", "in parse2");

                    //Getting json
                    userJsonObject = jsonArray.getJSONObject(i);

                    user.setUserEmail(userJsonObject.getString("userEmail"));
                    user.setUserID(Integer.parseInt(userJsonObject.getString("userID")));
                    user.setUserFirstName(userJsonObject.getString("userFirstName"));
                    user.setUserLastName(userJsonObject.getString("userLastName"));
                    user.setUserRating(Integer.parseInt(userJsonObject.getString("userRating")));
                    user.setUserResponse(Integer.parseInt(userJsonObject.getString("userResponse")));
                    user.setUserReliability(Integer.parseInt(userJsonObject.getString("userReputation")));
                    user.setFb(userJsonObject.getString("userFacebook"));
                    user.setInsta(userJsonObject.getString("userInstagram"));
                    user.setTwitter(userJsonObject.getString("userTwitter"));


                    Log.d("jadofferrs", "in parseEndSuccessful");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                requesterssList.add(i, requester);
            }
//
//        //Notifying the adapter that data has been added or changed
            adapter = new RequestersAdapter(requesterssList, this);
            recyclerView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void approve(final String requesterUserID) {
        progressBarCenter.setVisibility(View.VISIBLE);
        //Displaying Progressbar
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://kcapplications.com/Linc/teacher/approve.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressBarCenter.setVisibility(View.GONE);
                        Intent myIntent = new Intent(RequestersActivity.this, MainActivity.class);
                        startActivity(myIntent);
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressBarCenter.setVisibility(View.GONE);
                        if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), volleyError.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("user_id", requesterUserID);
                params.put("offer_id", "" + offer.getOfferID());
                return params;
            }
        };
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);

    }
}
