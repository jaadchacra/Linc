package com.fyp.atom.linc.Main.Chats;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fyp.atom.linc.Models.Approval;
import com.fyp.atom.linc.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ATOM on 11/30/2016.
 */

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {
    private Context context;
    List<Approval> approvalsList;

    //Constructor of this class
    public ChatsAdapter(List<Approval> approvalsList, Context context) {
        super();
        this.approvalsList = approvalsList;
        this.context = context;
    }

    @Override
    public ChatsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_chats, parent, false);

        ChatsAdapter.ViewHolder viewHolder = new ChatsAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChatsAdapter.ViewHolder holder, final int position) {
        //Getting the particular item from the list
        final Approval approval = approvalsList.get(position);

        String dateStr = approval.getDateStr();
        holder.textDate.setText("Meeting Date: "+dateStr);


        String approvalName = approval.getUser().getName();
        holder.textApproval.setText(approvalName);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/montserrat_regular_0.ttf");
        holder.textApproval.setTypeface(tf);

        String logoUrl = "http://kcapplications.com/Linc/images/" + approval.getUser().getUserEmail() +"_logo.jpg";

        Log.d("logoooo", logoUrl);
        Picasso.with(context)
                .load(logoUrl)
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewApproval);

        holder.relApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(context, ChatActivity.class);
                myIntent.putExtra("Approval", approval);
                context.startActivity(myIntent);
                ((Activity)context).overridePendingTransition(0, 0);
            }
        });

    }

    @Override
    public int getItemCount() {
        return approvalsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public RelativeLayout relApproval;
        public CircleImageView imageViewApproval;
        public TextView textApproval;
        public TextView textDate;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            relApproval = (RelativeLayout) itemView.findViewById(R.id.relApproval);
            imageViewApproval = (CircleImageView) itemView.findViewById(R.id.imageViewApproval);
            textApproval = (TextView) itemView.findViewById(R.id.textApproval);
            textDate = (TextView) itemView.findViewById(R.id.textDate);
        }
    }

}
