package com.fyp.atom.linc.Main.StudentFeed.CreateOffer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Main.StudentFeed.CreateOffer.OfferImage.OfferImageCropActivity;
import com.fyp.atom.linc.Models.Course;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Utils.SQLiteHandler;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class CreateOfferActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    private static final String EXTRA_FILE_PATH = "EXTRA_FILE_PATH";
    Offer offer;
    ProgressBar progressBarCenter;
    TextView errorMessage, toolbarText, dateSelectedTextView;
    EditText titleEditText, descriptionEditText, priceEditText, tagsEditText;
    Button continueButton, retryButton, imageButton, selectDateButton, addTagButton;
    ImageView offerImageView;
    Toolbar toolbar;
    Bundle globalSavedInstanceState;
    private SQLiteHandler db;
    ArrayList<Course> courseList;
    Spinner spinner;
    Spinner courseSpinner;
    DatePickerDialog datePickerDialog;
    TimePickerDialog timePickerDialog;
    LinearLayout container;

    //To post on server
    String offerPrice;
    String offerTitle;
    String offerDescription;
    String offerDateStr;
    ArrayList<String> offerTags = new ArrayList<>();
    String userID;
    String courseID;
    private Bitmap bitmap;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(0, 0);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_offer);
        globalSavedInstanceState = savedInstanceState;

        init();
        //To get the user from DB
        initSQLiteHandler();
        //To init date and time pickers
        initDateTimePickers();

        // Used to remove soft keyboard!!!!!
        setupUI(findViewById(R.id.rel_everything));

        //sets the image from cropping activity and stores the bitmap in our variable to send to server
        fromCropDiscountActivity();

        uploadButtonClickListener();

        imageButtonClickListener();

        selectDateButtonClickListener();

        addTagButtonClickListener();

        // Volley request to fill Categories Spinner
        getData(globalSavedInstanceState, "http://kcapplications.com/Linc/getCategories.php", "Categories");

        spinnerListener();
    }

    private void addTagButtonClickListener() {
        addTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(offerTags.size() == 3){
                    Toast.makeText(getApplicationContext(), "You can only have 3 Tags", Toast.LENGTH_LONG).show();
                } else if (tagsEditText.getText().toString().replace(" ", "").length() == 0) {
                    tagsEditText.setError("Please enter a tag first");
                } else{
                    LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.list_item_tag, null);
                    final TextView tagTextView = (TextView) addView.findViewById(R.id.tagTextView);

                    offerTags.add(tagsEditText.getText().toString().replaceAll(" ",""));

                    tagTextView.setText("#"+tagsEditText.getText().toString().replaceAll(" ",""));
                    tagsEditText.setText("");
                    tagsEditText.setError(null);

                    Button buttonRemove = (Button) addView.findViewById(R.id.remove);
                    buttonRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((LinearLayout) addView.getParent()).removeView(addView);
                            offerTags.remove(tagTextView.getText().toString());
                        }
                    });
                    container.addView(addView);
                }
            }
        });

    }

    private void selectDateButtonClickListener() {
        selectDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
            }
        });
    }

    private void initDateTimePickers() {
        Calendar now = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(CreateOfferActivity.this,  now.get(Calendar.YEAR),  now.get(Calendar.MONTH),  now.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setMinDate(now);
        Calendar nextWeek = Calendar.getInstance();
        nextWeek.add(Calendar.DAY_OF_MONTH, 7);
        datePickerDialog.setMaxDate(nextWeek);

        timePickerDialog = TimePickerDialog.newInstance(CreateOfferActivity.this, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), DateFormat.is24HourFormat(CreateOfferActivity.this));
    }

    private void initSQLiteHandler() {
        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        userID = user.get("uid");
//        Log.d("userserserser", user.get("name")+" "+user.get("uid"));
    }

    private void imageButtonClickListener() {
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), OfferImageCropActivity.class);
                //Used so we don't lose them, DiscountCropActivity sends them back here so we upload to server
                intent.putExtra("offer", offer);
                startActivity(intent);
                finish();
            }
        });
    }

    public void init(){
        offer = new Offer();

        progressBarCenter = (ProgressBar) findViewById(R.id.progressbar_center_courses);
        progressBarCenter.setVisibility(View.GONE);
        errorMessage = (TextView) findViewById(R.id.error_message_courses);
        retryButton = (Button) findViewById(R.id.retry_button_courses);
        continueButton = (Button) findViewById(R.id.continueButton);
        toolbarText = (TextView) findViewById(R.id.text_toolbar_courses);
        toolbar = (Toolbar) findViewById(R.id.toolbar_courses);
        imageButton = (Button) findViewById(R.id.imageButton);
        offerImageView = (ImageView) findViewById(R.id.offerImageView);
        dateSelectedTextView = (TextView) findViewById(R.id.dateSelectedTextView);
        selectDateButton = (Button) findViewById(R.id.selectDateButton);
        titleEditText = (EditText) findViewById(R.id.titleEditText);
        descriptionEditText = (EditText) findViewById(R.id.descriptionEditText);
        priceEditText = (EditText) findViewById(R.id.priceEditText);
        container = (LinearLayout) findViewById(R.id.container);
        tagsEditText = (EditText) findViewById(R.id.tagsEditText);
        addTagButton = (Button) findViewById(R.id.addTagButton);

        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);
        spinner = (Spinner) findViewById(R.id.my_spin);
        courseSpinner = (Spinner) findViewById(R.id.course_spin);
        spinner.setPrompt("Choose Category");
        courseSpinner.setPrompt("Choose Subject");
        courseSpinner.setEnabled(false);
        courseList = new  ArrayList<>();

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular_0.ttf");
        errorMessage.setTypeface(tf);
        retryButton.setTypeface(tf);
        toolbarText.setTypeface(tf);
        toolbarText.setText("Create an Offer");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void fromCropDiscountActivity() {
        //if calling activity is cropDiscountActivity put the cropped image
        String caller = getIntent().getStringExtra("caller");
        //Put the image here if it was cropped, otherwise keep imageView Invisible
        if (getIntent().hasExtra("caller")) {
            Log.d("where", "caller");
            if (caller.equals("OfferImageCropActivity")) {
                Log.d("where", "OfferImageCropActivity");
                if (!getIntent().getStringExtra(EXTRA_FILE_PATH).isEmpty()) {
                    Log.d("where", "EXTRA_FILE_PATH");
                    String filePath = getIntent().getStringExtra(EXTRA_FILE_PATH);
                    File imageFile = new File(filePath);
                    Uri uriFilePath = Uri.fromFile(imageFile);
                    try {
                        //Getting the Bitmap from Gallery
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriFilePath);

                        //Setting the Bitmap to ImageView
                        offerImageView.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                offer = (Offer) getIntent().getSerializableExtra("offer");
            }
        }
    }

    private void uploadButtonClickListener() {
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Checks if everything is available for submitting
                if(titleEditText.getText().toString().replaceAll(" ","").equals("")){
                    Toast.makeText(getApplicationContext(), "Please insert a title", Toast.LENGTH_LONG).show();
                } else if(descriptionEditText.getText().toString().replaceAll(" ","").equals("")){
                    Toast.makeText(getApplicationContext(), "Please insert a description", Toast.LENGTH_LONG).show();
                } else if(courseSpinner.isEnabled() == false){
                    Toast.makeText(getApplicationContext(), "Please select a category and course", Toast.LENGTH_LONG).show();
                } else if(spinner.getSelectedItemPosition() == 0 || spinner.getSelectedItemPosition() == -1){
                    Toast.makeText(getApplicationContext(), "Please select a category and course", Toast.LENGTH_LONG).show();
                } else if(courseSpinner.getSelectedItemPosition() == 0 || courseSpinner.getSelectedItemPosition() == -1){
                    Toast.makeText(getApplicationContext(), "Please select a course", Toast.LENGTH_LONG).show();
                } else if(priceEditText.getText().toString().replaceAll(" ","").equals("")){
                    Toast.makeText(getApplicationContext(), "Please provide a price", Toast.LENGTH_LONG).show();
                } else if(dateSelectedTextView.getText().toString().replaceAll(" ","").equals("")){
                    Toast.makeText(getApplicationContext(), "Please select a date", Toast.LENGTH_LONG).show();
                } else if(offerTags.size() == 0){
                    Toast.makeText(getApplicationContext(), "Please provide atleast one tag", Toast.LENGTH_LONG).show();
                } else{
                    continueButton.setEnabled(false);
                    for(int i=0; i<courseList.size(); i++){
                        if(courseSpinner.getSelectedItem().toString() == courseList.get(i).getName()){
                            courseID = ""+courseList.get(i).getCourseID();
                            break;
                        }
                    }


                    progressBarCenter.setVisibility(View.VISIBLE);

                    offerPrice = priceEditText.getText().toString();
                    offerTitle = titleEditText.getText().toString();
                    offerDescription = descriptionEditText.getText().toString();

                    postOffer(offerPrice, offerTitle, offerDescription, offerDateStr, offerTags, userID, courseID) ;
                }
            }
        });
    }

    private void postOffer(final String price, final String title, final String description, final String date, final ArrayList<String> tags, final String userId, final String courseId){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://kcapplications.com/Linc/offer.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        continueButton.setEnabled(true);
                        progressBarCenter.setVisibility(View.GONE);

                        Log.d("responsingz", s);
                        if(s.equals("") || s.equals("success")){
                            Toast.makeText(CreateOfferActivity.this, "Offer Created, you can see it in your Profile Tab", Toast.LENGTH_SHORT).show();
                            Intent myIntent = new Intent(CreateOfferActivity.this, MainActivity.class);
                            CreateOfferActivity.this.startActivity(myIntent);
                        }
                        else{
                            Toast.makeText(CreateOfferActivity.this, s, Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        continueButton.setEnabled(true);
                        progressBarCenter.setVisibility(View.GONE);
                        if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("offer_price", price);
                params.put("offer_title", title);
                params.put("offer_description", description);
                params.put("offer_start_date", date);
                for(int i =0; i< tags.size(); i++){
                    String key = "offer_tags["+i+"]";
                    params.put(key, tags.get(i));
                }
                params.put("user_id", userId);
                params.put("course_id", courseId);
                if(bitmap != null){
                    String image = getStringImage(bitmap);
                    params.put("image", image);
                }
                return params;
            }
        };
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
    private void spinnerListener() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (position != 0) {
                    courseSpinner.setEnabled(false);
                    getData(globalSavedInstanceState, "http://kcapplications.com/Linc/courses.php?category=" + spinner.getSelectedItem().toString(), "Courses");
                } else {
                    courseSpinner.setEnabled(false);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }

    public void setupUI(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(CreateOfferActivity.this);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }
    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (NullPointerException err) {

        }


    }

    private void getData(final Bundle savedInstanceState, String url, String listName) {
        MySingleton.getInstance(this).addToRequestQueue(getDataFromServer(savedInstanceState, url, listName));
    }
    private JsonArrayRequest getDataFromServer(final Bundle savedInstanceState, final String url, final String listName) {

        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if (response != null) {
                            parseData(response, listName);
                        } else {
                            getData(globalSavedInstanceState, url, listName);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getData(globalSavedInstanceState, url, listName);
                    }
                });
        return jsonArrayRequest;
    }
    private void parseData(JSONArray array, String listName) {
        ArrayList<String> list = new ArrayList<String>();
        if (listName == "Courses") {
            list.add(0, "Choose Subject");
        } else {
            list.add(0, "Choose Category");
        }

        for (int i = 0; i < array.length(); i++) {
            Course myCourse = new Course();
            String course = "";
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);
//                //Getting the course
                if (listName == "Courses") {
                    course = json.getString("courseName");
                    myCourse.setName(course);
                    myCourse.setCourseID(json.getInt("courseID"));
                } else {
                    course = json.getString("categoryName");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            list.add(i + 1, course);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
            if (listName == "Courses") {
                courseList.add(i, myCourse);
                courseSpinner.setAdapter(adapter);
                courseSpinner.setEnabled(true);
            } else {
                spinner.setAdapter(adapter);
            }
        }
//        //Notifying the adapter that data has been added or changed
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        //2017-04-15 12:00:00
        String yearStr = String.format("%02d", year);
        String monthOfYearStr = String.format("%02d", monthOfYear+1);
        String dayOfMonthStr = String.format("%02d", dayOfMonth);

        offerDateStr = yearStr+"-"+monthOfYearStr+"-"+dayOfMonthStr+" ";

        timePickerDialog.show(getFragmentManager(), "TimeFragment");

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
            String hourOfDayStr = String.format("%02d", hourOfDay);
            String minuteStr = String.format("%02d", minute);
            String secondStr = String.format("%02d", second);

            offerDateStr += hourOfDayStr + ":" + minuteStr + ":" + secondStr;

            try {
                String displayDateStr = getDateStr(offerDateStr);
                dateSelectedTextView.setText(displayDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
    }

    public String getDateStr(String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = sdf.parse(dateStr);
        offer.setOfferStartDate(startDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        SimpleDateFormat formatter = new SimpleDateFormat("EEE");
        String dayOfWeekStr = formatter.format(cal.getTime());
        int startHour = cal.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        String startHourStr = String.format("%02d", startHour);

        int startMinute = cal.get(Calendar.MINUTE);
        String startMinuteStr = String.format("%02d", startMinute);

        int endHour = startHour + 1;
        if(endHour == 25)
            endHour = 0;
        String endHourStr = String.format("%02d", endHour);

        String offerDate =  dayOfWeekStr+" from "+startHourStr+":"+startMinuteStr+" till "+endHourStr+":"+startMinuteStr;
        return offerDate;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

}




