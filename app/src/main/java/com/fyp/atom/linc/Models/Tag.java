package com.fyp.atom.linc.Models;

import java.io.Serializable;

/**
 * Created by ATOM on 3/22/2017.
 */

public class Tag implements Serializable {
    String name;
    int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
