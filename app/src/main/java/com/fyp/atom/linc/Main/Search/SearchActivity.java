package com.fyp.atom.linc.Main.Search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.fyp.atom.linc.Main.Categories.Courses.Offers.OffersActivity;
import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Models.Name;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.TinyDB;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.overridePendingTransition(0, 0);
                this.finish();
//                this.overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
//                overridePendingTransition(android.R.anim.slide_out_right, android.R.anim.slide_in_left);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setDisplayShowCustomEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TinyDB tinyDB = new TinyDB(this);
        String[] searchShopsArray = tinyDB.getListString("allCourses").toArray(new String[tinyDB.getListString("allCourses").size()]);
        List<Name> searchShops = new ArrayList<>();;

        for(int i=0; i< searchShopsArray.length; i++){
            Name temp = new Name();
            temp.setName(searchShopsArray[i]);

            searchShops.add(i,temp);

        }


//        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_dropdown_item_1line, searchShops );
        final AutoCompleteTextView textView = (AutoCompleteTextView) this.findViewById(R.id.autocompleteView);
//        textView.setAdapter(adapter);

        Log.d("WHYYYYYY", searchShops.toString());

//        List<Names> namesList =
        NamesAdapter namesAdapter = new NamesAdapter(getBaseContext(), R.layout.activity_search, R.id.lbl_name, searchShops );
        //set adapter into listStudent
        textView.setAdapter(namesAdapter);
//        textView.showDropDown();

        textView.requestFocus();
//        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
        textView.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(textView, InputMethodManager.SHOW_IMPLICIT);
            }
        }, 250);

        textView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if((event.getRawX()) <= (textView.getCompoundDrawables()[0].getBounds().width()+textView.getLeft()))  {
                        // your action here
//                        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                        imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);

                        Intent myIntent = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(myIntent);
                        overridePendingTransition(0, 0);
                        return true;
                    }
                }
                return false;
            }
        });

        textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
                //... your stuff
                //TODO: change to tutorsList activity
                String name = textView.getText().toString();
                Intent myIntent = new Intent(getBaseContext(), OffersActivity.class);
                myIntent.putExtra("name", name);

                //TODO: if logo is .png then this is error!!!!!!! FIIXXXXXXXX
                String logo = "http://kcapplications.com/images/"+ name + "small_logo.jpg";
                myIntent.putExtra("courseName", name);
                myIntent.putExtra("logo", logo);
                myIntent.putExtra("search", true);
                startActivity(myIntent);
            }
        });



    }
}
