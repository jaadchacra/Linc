package com.fyp.atom.linc.Models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ATOM on 3/20/2017.
 */

public class Session implements Serializable {
    int sessionID;
    String name;
    int sessionHighestPrice;
    User user;
    List<Offer> offer;

    public int getSessionID() {
        return sessionID;
    }

    public void setSessionID(int sessionID) {
        this.sessionID = sessionID;
    }

    public List<Offer> getOffer() {
        return offer;
    }

    public void setOffer(List<Offer> offer) {
        this.offer = offer;
    }


    public int getSessionHighestPrice() {
        return sessionHighestPrice;
    }

    public void setSessionHighestPrice(int sessionHighestPrice) {
        this.sessionHighestPrice = sessionHighestPrice;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
