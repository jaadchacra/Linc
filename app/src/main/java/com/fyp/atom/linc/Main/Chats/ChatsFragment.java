package com.fyp.atom.linc.Main.Chats;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Main.Sessions.SessionsAdapter;
import com.fyp.atom.linc.Models.Approval;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.Models.Session;
import com.fyp.atom.linc.Models.User;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Utils.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ChatsFragment extends Fragment {
    private SQLiteHandler db;
    String userID;
    View view;
    Bundle mSavedInstanceState;
    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter chatsAdapter;
    private List<Approval> approvalsList;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {

            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chats, container, false);
        //This is basically never used, it's just to use getData from onScrolled function
        mSavedInstanceState = savedInstanceState;

        init();

        try {
            getChats(mSavedInstanceState);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        return view;
    }

    private void init() {
        db = new SQLiteHandler(getActivity().getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        userID = user.get("uid");

        progressBarCenter = (ProgressBar) view.findViewById(R.id.progressBarCenter);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) view.findViewById(R.id.studentFeedErrorMessage);
        retryButton = (Button) view.findViewById(R.id.studentFeedRetryButton);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        approvalsList = new ArrayList<>();

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }





    private String dateStrNow() {
        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowStr = sdf.format(now.getTime());
        return nowStr;
    }
    private void getChats(final Bundle savedInstanceState) throws UnsupportedEncodingException {
        MySingleton.getInstance(getActivity()).addToRequestQueue(getChatsFromServer(savedInstanceState));
    }
    private StringRequest getChatsFromServer(final Bundle savedInstanceState) throws UnsupportedEncodingException {
        //Displaying Progressbar
        progressBarCenter.setVisibility(View.VISIBLE);
        String myDate = URLEncoder.encode(dateStrNow(), "utf-8");;

        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, " http://kcapplications.com/Linc/chat.php?user_id="+userID+"&date="+myDate,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("sessions", response.toString());
                        if (response != null) {
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            try {
                                getChats(savedInstanceState);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("sessions", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                try {
                                    getChats(savedInstanceState);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
        return postRequest;
    }
    //This method will parse json data
    private void parseData(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                Approval approval = new Approval();
                User user = new User();

                JSONObject approvalJsonObject = null;

                try {
                    //Getting json
                    approvalJsonObject = jsonArray.getJSONObject(i);

                    user.setUserID(Integer.parseInt(approvalJsonObject.getString("userID")));
                    user.setUserFirstName(approvalJsonObject.getString("userFirstName"));
                    user.setUserLastName(approvalJsonObject.getString("userLastName"));
                    user.setUserEmail(approvalJsonObject.getString("userEmail"));

                    approval.setUser(user);
                    approval.setDateStr(getDateStr(approvalJsonObject.getString("offerStartDate")));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                approvalsList.add(0, approval);
            }
//        //Notifying the adapter that data has been added or changed
            chatsAdapter = new ChatsAdapter(approvalsList, getActivity());
            recyclerView.setAdapter(chatsAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getDateStr(String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = sdf.parse(dateStr);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        SimpleDateFormat formatter = new SimpleDateFormat("EEE");
        String dayOfWeekStr = formatter.format(cal.getTime());
        int startHour = cal.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        String startHourStr = String.format("%02d", startHour);

        int startMinute = cal.get(Calendar.MINUTE);
        String startMinuteStr = String.format("%02d", startMinute);

        int endHour = startHour + 1;
        if(endHour == 25)
            endHour = 0;
        String endHourStr = String.format("%02d", endHour);

        String offerDate =  dayOfWeekStr+" from "+startHourStr+":"+startMinuteStr+" till "+endHourStr+":"+startMinuteStr;
        return offerDate;
    }


}
