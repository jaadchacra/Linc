package com.fyp.atom.linc.Main.Categories.Courses;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fyp.atom.linc.Main.Categories.Courses.Offers.OffersActivity;
import com.fyp.atom.linc.Models.Course;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.TinyDB;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;


public class CoursesAdapter extends RecyclerView.Adapter<CoursesAdapter.ViewHolder> {

    private Context context;
    TinyDB tinyDB;


//    ImageButton retryButtonDiscount = null;

    //List to store all superheroes
    List<Course> coursesList;

    //Constructor of this class
    public CoursesAdapter(List<Course> coursesList, Context context) {
        super();
        //Getting all superheroes
        this.coursesList = coursesList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_courses, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        //Getting the particular item from the list
        //lowercase is for the url
        final Course course = coursesList.get(position);
        String courseName = course.getName().toLowerCase();

        holder.textViewCourse.setText(courseName.toUpperCase());
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/montserrat_regular_0.ttf");
        holder.textViewCourse.setTypeface(tf);

//        String uri = "@drawable/" + courseName.toLowerCase() + "_bar";  // where myresource (without the extension) is the file
//        int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
//        Drawable res = context.getResources().getDrawable(imageResource);
//        holder.imageViewCourse.setImageDrawable(res);

        try {
            courseName = URLEncoder.encode(courseName.replaceAll(" ", "_"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String logoUrl = "http://kcapplications.com/Linc/images/"+ courseName +".png";

        Log.d("logoUrl", logoUrl);

        final String categoryName = ((CoursesActivity)context).getCategoryName();
        Log.d("categoryName", categoryName);
        if(categoryName.equals("Tutoring")){
            String uri = "@drawable/" + "default_course";  // where myresource (without the extension) is the file
            int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
            Drawable res = context.getResources().getDrawable(imageResource);
            holder.imageViewCourse.setImageDrawable(res);
        } else if (categoryName.equals("Random")){

            if(course.getCategory_Name().equals("Tutoring")) {
                String uri = "@drawable/" + "default_course";  // where myresource (without the extension) is the file
                int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
                Drawable res = context.getResources().getDrawable(imageResource);
                holder.imageViewCourse.setImageDrawable(res);
            } else{
                Picasso.with(context)
                        .load(logoUrl)
                        .placeholder(R.drawable.placeholder)
                        .into(holder.imageViewCourse);
            }
        } else {
            Picasso.with(context)
                    .load(logoUrl)
                    .placeholder(R.drawable.placeholder)
//                .fit()
                    .into(holder.imageViewCourse, new ImageLoadedCallback() {
                        @Override
                        public void onSuccess() {
                            if (position == coursesList.size() - 1) {
//                            tinyDB = new TinyDB(context);
//                            String categoryName = ((CoursesActivity)context).getCategoryName();
//                            tinyDB.putBoolean("loaded_" + categoryName, true);
//                            ((CoursesActivity) context).getProgressBarCenter().setVisibility(View.GONE);
//                            ((CoursesActivity) context).getAdapter().notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onError() {
                            if (position == coursesList.size() - 1) {

                            }

                        }
                    });
        }

//                .memoryPolicy(MemoryPolicy.NO_CACHE )
//                .networkPolicy(NetworkPolicy.NO_CACHE)

        holder.relCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO: make intent to activity or reload a new fragment??
                //TODO: get activity instead of context so it doesn't open second instance of app? or is this only because of my phone? :s, test on other phone
                Intent myIntent = new Intent(context, OffersActivity.class);
                myIntent.putExtra("categoryName", categoryName);

                myIntent.putExtra("courseName", course.getName());
//                myIntent.putExtra("logo", logo);
//
                context.startActivity(myIntent);
                ((Activity)context).overridePendingTransition(0, 0);


            }
        });

        Log.d("aaa", position+"");
        //Adding separators between courses
//        if(position > 0) {
            holder.horizontalLine.setVisibility(View.VISIBLE);
//            ImageView imageSeparator = new ImageView(context);
//            imageSeparator.setLayoutParams(new RelativeLayout.LayoutParams(
//                    RelativeLayout.LayoutParams.MATCH_PARENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT));
//            imageSeparator.setPadding(getPx(5), getPx(10), getPx(5), getPx(10));
//            imageSeparator.setImageDrawable(context.getResources().getDrawable(android.R.drawable.divider_horizontal_bright));
//            imageSeparator.setScaleType(ImageView.ScaleType.FIT_XY);
//
//            holder.relCourse.addView(imageSeparator);
//        } else{
//            holder.horizontalLine.setVisibility(View.GONE);
//        }
    }

    @Override
    public int getItemCount() {
        return coursesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public ImageView imageViewCourse, horizontalLine;
        public RelativeLayout relCourse;
        public TextView textViewCourse;


        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            relCourse = (RelativeLayout) itemView.findViewById(R.id.rel_course);
            imageViewCourse = (ImageView) itemView.findViewById(R.id.image_course);
            textViewCourse = (TextView) itemView.findViewById(R.id.text_course);
            horizontalLine = (ImageView) itemView.findViewById(R.id.horizontal_line_courses);

//
        }
    }

    public int getPx(int dimensionDp) {
        float density = context.getResources().getDisplayMetrics().density;
        return (int) (dimensionDp * density + 0.5f);
    }

    private class ImageLoadedCallback implements Callback {
//        ImageButton retryButton;

        public ImageLoadedCallback() {
//            this.retryButton = retryButton;

        }

//        public  ImageLoadedCallback(ProgressBar progBar, ImageButton retryButton){
//            progressBar = progBar;
//            this.retryButton = retryButton;
//
//        }


        @Override
        public void onSuccess() {

        }

        @Override
        public void onError() {

        }
    }

}