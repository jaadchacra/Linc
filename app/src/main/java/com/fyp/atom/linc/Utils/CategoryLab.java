package com.fyp.atom.linc.Utils;


import android.content.Context;

import com.fyp.atom.linc.Models.Category;

import java.util.ArrayList;
import java.util.List;

/*
* We added this again cause we removed the Women, Men, and Kids Fragment and went back to one
* */
public class CategoryLab {
    private static CategoryLab staticCategoryLab;

    int id=0;

    private ArrayList<Category> mCategories;

    public static CategoryLab get(Context context) {
        if (staticCategoryLab == null) {
            staticCategoryLab = new CategoryLab(context);
        }
        return staticCategoryLab;
    }

    private CategoryLab(Context context) {
        mCategories = new ArrayList<>();
        Category sports = new Category();
        sports.setCategoryName("Sports");
        sports.setDrawableName("sports");
        sports.setId(id);
        mCategories.add(id, sports);
        id++;

        Category arts = new Category();
        arts.setCategoryName("Arts");
        arts.setDrawableName("arts");
        arts.setId(id);
        mCategories.add(id, arts);
        id++;

        Category music = new Category();
        music.setCategoryName("Music");
        music.setDrawableName("music");
        music.setId(id);
        mCategories.add(id, music);
        id++;

        Category languages = new Category();
        languages.setCategoryName("Languages");
        languages.setDrawableName("languages");
        languages.setId(id);
        mCategories.add(id, languages);
        id++;

        Category tutoring = new Category();
        tutoring.setCategoryName("Tutoring");
        tutoring.setDrawableName("tutoring");
        tutoring.setId(id);
        mCategories.add(id, tutoring);
        id++;

        Category random = new Category();
        random.setCategoryName("Random");
        random.setDrawableName("random");
        random.setId(id);
        mCategories.add(id, random);
        id++;

    }

    public List<Category> getCategories() {
        return mCategories;
    }

}