package com.fyp.atom.linc.Models;

import java.io.Serializable;

/**
 * Created by ATOM on 11/18/2016.
 */

public class Name implements Serializable {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String name;
}