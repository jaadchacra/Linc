package com.fyp.atom.linc.Main.Categories.Courses;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Models.Course;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Utils.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CoursesActivity extends AppCompatActivity {
    private SQLiteHandler db;
    String userID;
    String categoryName = "";
    ProgressBar progressBarBottom, progressBarCenter;
    TextView shopName, shopCoverName, errorMessage, toolbarText;
    ImageView categoryBarImg;
    Button retryButton;
    Toolbar toolbar;
    List<Course> CoursesList;
    Bundle globalSavedInstanceState;
    private RecyclerView.Adapter adapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                //I REMOVED THIS AND PUT (0,0) !!!, there was one also in onCreate for when we enter the app
//                this.overridePendingTransition(R.anim.animation_leave2,R.anim.animation_enter2);
                overridePendingTransition(0, 0);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses);
        globalSavedInstanceState = savedInstanceState;

        CoursesList = new ArrayList<>();
        Log.d("ActivityNow", "I'm in CoursesActivity!");

        db = new SQLiteHandler(this.getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        userID = user.get("uid");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_courses);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        progressBarCenter = (ProgressBar) findViewById(R.id.progressbar_center_courses);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) findViewById(R.id.error_message_courses);
        retryButton = (Button) findViewById(R.id.retry_button_courses);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        toolbarText = (TextView) findViewById(R.id.text_toolbar_courses);

        //initializing our adapter
        adapter = new CoursesAdapter(CoursesList, this);

        //Adding adapter to recyclerview
        recyclerView.setAdapter(adapter);


        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular_0.ttf");
        errorMessage.setTypeface(tf);
        retryButton.setTypeface(tf);
        toolbarText.setTypeface(tf);

        Intent mIntent = getIntent();
        categoryName = mIntent.getExtras().getString("categoryName");

        toolbarText.setText(categoryName);

        if(categoryName.equals("Random"))
            toolbarText.setText("Recommended");

        String categoryBarImgName = categoryName;
        categoryBarImgName = categoryBarImgName.toLowerCase() + "_bar";
        Log.d("aaaa", categoryBarImgName);
        int resId = getResources().getIdentifier(categoryBarImgName, "drawable", getPackageName());
        toolbar = (Toolbar) findViewById(R.id.toolbar_courses);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setBackgroundResource(resId);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        getCourses(globalSavedInstanceState);
//
    }



    private void getCourses(final Bundle savedInstanceState) {
        MySingleton.getInstance(this).addToRequestQueue(getCoursesFromServer(savedInstanceState));
    }

    private JsonArrayRequest getCoursesFromServer(final Bundle savedInstanceState) {
        //Displaying Progressbar
        progressBarCenter.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://kcapplications.com/Linc/courses.php?user_id="+ userID + "&category=" + categoryName,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBarCenter.setVisibility(View.GONE);

                        Log.d("response", response.toString());
                        if(response != null){
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getCourses(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else{
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getCourses(savedInstanceState);
                            }
                        });
                    }
                });
        return jsonArrayRequest;
    }

    //This method will parse json data
    private void parseData(JSONArray array) {

        for (int i = 0; i < array.length(); i++) {
            Course course = new Course();
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);
//
//                //Getting the course
                course.setName(json.getString("courseName"));
                course.setCategory_Name(json.getString("categoryName"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
                CoursesList.add(0, course);
        }
//
//        //Notifying the adapter that data has been added or changed
        adapter.notifyDataSetChanged();
    }

    public String getCategoryName(){
        return categoryName;
    }

}




