package com.fyp.atom.linc.Registration.SocialMedia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Login.LoginActivity;
import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Main.StudentFeed.CreateOffer.CreateOfferActivity;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Registration.ProfileImage.ImageViewActivity;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Utils.SQLiteHandler;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class SocialMediaActivity extends AppCompatActivity {

    private SQLiteHandler db;
    EditText etFacebook;
    EditText etTwitter;
    EditText etInstagram;
    Button profilePictureButton, continueButton, retryButton;
    String userFacebook, userTwitter, userInstagram, userID;
    ProgressBar progressBarCenter;
    TextView errorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_media);

        init();

        ppButtonClickListener();

        continueButtonClickListener();


    }



    private void init() {
        db = new SQLiteHandler(this);
        HashMap<String, String> hashMap = db.getUserDetails();

        progressBarCenter = (ProgressBar) findViewById(R.id.progressBarCenter);
        errorMessage = (TextView) findViewById(R.id.errorMessage);
        retryButton = (Button) findViewById(R.id.retryButton);
        progressBarCenter.setVisibility(View.GONE);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        etFacebook = (EditText) findViewById(R.id.etFacebook);
        etTwitter = (EditText) findViewById(R.id.etTwitter);
        etInstagram = (EditText) findViewById(R.id.etInstagram);
        profilePictureButton = (Button) findViewById(R.id.profilePictureButton);
        continueButton = (Button) findViewById(R.id.continueButton);

        userFacebook = hashMap.get("userFacebook");
        userTwitter = hashMap.get("userTwitter");
        userInstagram = hashMap.get("userInstagram");
        userID = hashMap.get("uid");

        etFacebook.setText(userFacebook);
        etTwitter.setText(userTwitter);
        etInstagram.setText(userInstagram);
    }


    private void ppButtonClickListener() {
        profilePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!userFacebook.equals(etFacebook.getText().toString()) ||
                        !userTwitter.equals(etTwitter.getText().toString()) ||
                        !userInstagram.equals(etInstagram.getText().toString())){
                    uploadSocialMedia(true);
                } else{
                    Intent intent = new Intent(SocialMediaActivity.this, ImageViewActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

    }



    private void continueButtonClickListener() {
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!userFacebook.equals(etFacebook.getText().toString()) ||
                        !userTwitter.equals(etTwitter.getText().toString()) ||
                        !userInstagram.equals(etInstagram.getText().toString())){
                    uploadSocialMedia(false);
                } else{
                    Intent intent = new Intent(SocialMediaActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }


    private void uploadSocialMedia(final boolean bool) {
        progressBarCenter.setVisibility(View.VISIBLE);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://kcapplications.com/Linc/socialmedia.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressBarCenter.setVisibility(View.GONE);

                        Log.d("responsingz", s);
                        if(bool == false){
                            Intent myIntent = new Intent(SocialMediaActivity.this, MainActivity.class);
                            startActivity(myIntent);
                            finish();
                        }else{
                            Intent myIntent = new Intent(SocialMediaActivity.this, ImageViewActivity.class);
                            startActivity(myIntent);
                            finish();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressBarCenter.setVisibility(View.GONE);
                        if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("facebook", etFacebook.getText().toString());
                params.put("instagram", etInstagram.getText().toString());
                params.put("twitter", etTwitter.getText().toString());
                params.put("user_id", userID);

                return params;
            }
        };
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
