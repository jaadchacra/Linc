package com.fyp.atom.linc.Registration.LearningInterestsActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.R;

public class LearningInterestsActivity extends AppCompatActivity {
    Button continueButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learning_interests);
        continueButton = (Button) findViewById(R.id.continueButton);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(LearningInterestsActivity.this, MainActivity.class);
                LearningInterestsActivity.this.startActivity(myIntent);
            }
        });
    }
}
