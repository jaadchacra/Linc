package com.fyp.atom.linc.Main.Sessions.SessionDetails;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Main.StudentFeed.StudentFeedAdapter;
import com.fyp.atom.linc.Models.Category;
import com.fyp.atom.linc.Models.Course;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.Models.Session;
import com.fyp.atom.linc.Models.Tag;
import com.fyp.atom.linc.Models.User;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SessionDetailsActivity extends AppCompatActivity {
    Bundle mSavedInstanceState;
    Toolbar sessionDetailsToolbar;
    TextView toolbarTextView;
    private List<Offer> offersList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter sessionDetailsAdapter;
    Session session;
    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(0, 0);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_details);
//This is basically never used, it's just to use getData from onScrolled function
        mSavedInstanceState = savedInstanceState;

        init();

        getOffers(mSavedInstanceState);

    }

    private void init() {
        Intent mIntent = getIntent();
        session = (Session) mIntent.getSerializableExtra("session");
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        progressBarCenter = (ProgressBar) findViewById(R.id.progressBarCenter);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) findViewById(R.id.errorMessageTextView);
        retryButton = (Button) findViewById(R.id.retryButton);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        toolbarTextView = (TextView) findViewById(R.id.toolbarTextView);
        sessionDetailsToolbar = (Toolbar) findViewById(R.id.sessionDetailsToolbar);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular_0.ttf");
        toolbarTextView.setTypeface(tf);
        toolbarTextView.setText("Waiting List: "+session.getName());
        setSupportActionBar(sessionDetailsToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setSupportActionBar(sessionDetailsToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        offersList = new ArrayList<>();
    }


    private void getOffers(final Bundle savedInstanceState) {
        MySingleton.getInstance(this).addToRequestQueue(getOffersFromServer(savedInstanceState));
    }
    private StringRequest getOffersFromServer(final Bundle savedInstanceState) {
        //Displaying Progressbar
        progressBarCenter.setVisibility(View.VISIBLE);
        String myDate = dateStrNow();

        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, "http://kcapplications.com/Linc/session/offers.php?session_id="+session.getSessionID()+"&date="+myDate,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("offerrs", response.toString());
                        if (response != null) {
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getOffers(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("offerrs", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getOffers(savedInstanceState);
                            }
                        });
                    }
                });
        return postRequest;
    }
    private String dateStrNow() {
        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowStr = sdf.format(now.getTime());
        return nowStr;
    }
    //This method will parse json data
    private void parseData(String response) {


        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                Offer offer = new Offer();
                User user = new User();
                Course course = new Course();
                Category category = new Category();

                offer.setUser(user);
                offer.setCourse(course);
                offer.setCategory(category);

                JSONObject offerJsonObject = null;
                JSONObject userJsonObject = null;
                JSONObject courseJsonObject = null;
                JSONObject categoryJsonObject = null;
                JSONArray tagJsonArray = null;

                try {
                    Log.d("jadofferrs", "in parse");
                    Log.d("jadofferrs", "in parse2");

                    //Getting json
                    offerJsonObject = jsonArray.getJSONObject(i);
                    userJsonObject = offerJsonObject.getJSONObject("User");
                    courseJsonObject = offerJsonObject.getJSONObject("Course");;
                    categoryJsonObject = offerJsonObject.getJSONObject("Category");;
                    tagJsonArray = offerJsonObject.getJSONArray("Tag");
                    ArrayList<Tag> tagsList = new ArrayList<>();
                    if (tagJsonArray != null) {
                        for (int j=0;j<tagJsonArray.length();j++){
                            Tag tag = new Tag();
                            tag.setName(tagJsonArray.getJSONObject(j).getString("tagName"));
                            tagsList.add(tag);
                            Log.d("jadofferrs", tag.getName());
                        }
                    }

                    Log.d("jadofferrs", "in parse2");

//                  Getting the offer


                    offer.setTitle(offerJsonObject.getString("offerTitle"));
                    offer.getCourse().setName(courseJsonObject.getString("courseName"));
                    offer.setDescription(offerJsonObject.getString("offerDescription"));
                    offer.setPrice(offerJsonObject.getInt("offerPrice"));

                    String dateStr = offerJsonObject.getString("offerStartDate");
                    offer.setDateStr(getDateStr(offer, dateStr));

                    offer.setTagsList(tagsList);

                    offer.getUser().setUserEmail(userJsonObject.getString("userEmail"));
                    offer.getUser().setUserFirstName(userJsonObject.getString("userFirstName"));
                    offer.getUser().setUserLastName(userJsonObject.getString("userLastName"));
                    offer.getUser().setUserRating(userJsonObject.getInt("userRating"));
                    offer.getUser().setUserResponse(userJsonObject.getInt("userResponse"));
                    offer.getUser().setUserReliability(userJsonObject.getInt("userReputation"));


                    offer.getCategory().setCategoryName(categoryJsonObject.getString("categoryName"));



                    Log.d("jadofferrs", "in parseEndSuccessful");

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                offersList.add(i, offer);
            }
//
//        //Notifying the adapter that data has been added or changed
            sessionDetailsAdapter = new StudentFeedAdapter(offersList, this);
            recyclerView.setAdapter(sessionDetailsAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String getDateStr(Offer offer, String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = sdf.parse(dateStr);
        offer.setOfferStartDate(startDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        SimpleDateFormat formatter = new SimpleDateFormat("EEE");
        String dayOfWeekStr = formatter.format(cal.getTime());
        int startHour = cal.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        String startHourStr = String.format("%02d", startHour);

        int startMinute = cal.get(Calendar.MINUTE);
        String startMinuteStr = String.format("%02d", startMinute);

        int endHour = startHour + 1;
        if(endHour == 25)
            endHour = 0;
        String endHourStr = String.format("%02d", endHour);

        String offerDate =  dayOfWeekStr+" from "+startHourStr+":"+startMinuteStr+" till "+endHourStr+":"+startMinuteStr;
        return offerDate;
    }
}
