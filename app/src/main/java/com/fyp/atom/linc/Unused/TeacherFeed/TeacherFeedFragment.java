package com.fyp.atom.linc.Unused.TeacherFeed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.R;

import java.util.ArrayList;
import java.util.List;

public class TeacherFeedFragment extends Fragment {
    View view;
    Bundle mSavedInstanceState;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter teacherFeedAdapter;
    private List<Offer> offersList;
    @Override
    public void onResume() {
        super.onResume();
        // put your code here...
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_teacherfeed, container, false);
        //This is basically never used, it's just to use getData from onScrolled function
        mSavedInstanceState = savedInstanceState;


        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        offersList = new ArrayList<>();

        populateList();

        Log.d("offer", "TeacherFeedAdapter");
        teacherFeedAdapter = new TeacherFeedAdapter(offersList, getActivity());
        recyclerView.setAdapter(teacherFeedAdapter);
        return view;
    }

    public void populateList(){
        Offer offer1 = new Offer();
        Offer offer2 = new Offer();
        Offer offer3 = new Offer();
        Offer offer4 = new Offer();
        Offer offer5 = new Offer();
        Offer offer6 = new Offer();
        Offer offer7 = new Offer();
        Offer offer8 = new Offer();
        Offer offer9 = new Offer();

        offer1.setFb_link("offer1");
        offer2.setFb_link("offer2");
        offer3.setFb_link("offer3");
        offer4.setFb_link("offer4");
        offer5.setFb_link("offer5");
        offer6.setFb_link("offer6");
        offer7.setFb_link("offer7");
        offer8.setFb_link("offer8");
        offer9.setFb_link("offer9");

        offersList.add(offer1);
        offersList.add(offer2);
        offersList.add(offer3);
        offersList.add(offer4);
        offersList.add(offer5);
        offersList.add(offer6);
        offersList.add(offer7);
        offersList.add(offer8);
        offersList.add(offer9);
    }

    // used when we detect that a discount was deleted
    private void reloadFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }

}

