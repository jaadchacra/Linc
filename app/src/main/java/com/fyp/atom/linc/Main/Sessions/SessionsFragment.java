package com.fyp.atom.linc.Main.Sessions;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.fyp.atom.linc.R;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Models.Session;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Utils.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class SessionsFragment extends Fragment {
    View view;
    Bundle mSavedInstanceState;
    private SQLiteHandler db;
    String userID;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter sessionAdapter;
    private List<Session> sessionList;
    ImageView addSessionButton;
    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_sessions, container, false);
        //This is basically never used, it's just to use getData from onScrolled function
        mSavedInstanceState = savedInstanceState;

        init();

        initSQLHandler();

        getSessions(mSavedInstanceState);

        addSessionButtonClickListener();

        return view;
    }

    private void initSQLHandler() {
        // SQLite database handler
        db = new SQLiteHandler(getActivity().getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        userID = user.get("uid");
//        Log.d("userserserser", user.get("name")+" "+user.get("uid"));
    }

    private void addSessionButtonClickListener() {
        addSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText txtUrl = new EditText(getContext());
                txtUrl.setHint("ex: Music Related or Monday Free Time");
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setView(txtUrl)
                        .setTitle("Add a WishList")
                        .setMessage("Name your WishList")
                        .setCancelable(true)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            // TODO: Call function to Add the session AND Notify change for adapter

                                addSession(mSavedInstanceState, txtUrl.getText().toString());
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    private void init() {
        addSessionButton = (ImageView) view.findViewById(R.id.addSessionButton);

        progressBarCenter = (ProgressBar) view.findViewById(R.id.progressBarCenter);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) view.findViewById(R.id.errorMessageTextView);
        retryButton = (Button) view.findViewById(R.id.retryButton);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

//        TextView textView = (TextView)view.findViewById(R.id.tv_student);
//        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/montserrat_regular_0.ttf");
//        textView.setTypeface(tf);
//        textView.setText("Insert Student Feed Here (Testing Typeface)");

        sessionList = new ArrayList<Session>();

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void getSessions(final Bundle savedInstanceState) {
        MySingleton.getInstance(getActivity()).addToRequestQueue(getSessionsFromServer(savedInstanceState));
    }
    private StringRequest getSessionsFromServer(final Bundle savedInstanceState) {
        //Displaying Progressbar
        progressBarCenter.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, "http://kcapplications.com/Linc/sessions.php?user_id="+userID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("sessions", response.toString());
                        if (response != null) {
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getSessions(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("sessions", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getSessions(savedInstanceState);
                            }
                        });
                    }
                });
        return postRequest;
    }
    //This method will parse json data
    private void parseData(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                Session session = new Session();

                JSONObject sessionJsonObject = null;

                String sessionStatus = "";

                try {
                    //Getting json
                    sessionJsonObject = jsonArray.getJSONObject(i);
                    session.setSessionID(Integer.parseInt(sessionJsonObject.getString("sessionID")));
                    session.setName(sessionJsonObject.getString("sessionName"));
                    if (sessionJsonObject.getString("sessionHighestPrice") != "null")
                        session.setSessionHighestPrice(Integer.parseInt(sessionJsonObject.getString("sessionHighestPrice")));
                    sessionStatus = sessionJsonObject.getString("sessionStatus");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(sessionStatus.equals("0"))
                    sessionList.add(0, session);
            }
//        //Notifying the adapter that data has been added or changed
            sessionAdapter = new SessionsAdapter(sessionList, getActivity());
            recyclerView.setAdapter(sessionAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addSession(final Bundle savedInstanceState, String sessionName) {
        MySingleton.getInstance(getActivity()).addToRequestQueue(addSessionToServer(savedInstanceState, sessionName));
    }

    private StringRequest addSessionToServer(Bundle savedInstanceState, final String sessionName) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://kcapplications.com/Linc/session.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        reloadFragment();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressBarCenter.setVisibility(View.GONE);
                        if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getActivity().getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof NetworkError) {
                            Toast.makeText(getActivity().getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getActivity().getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("session_name", sessionName);
                params.put("user_id", userID);
                return params;
            }
        };
        return stringRequest;

    }
    private void reloadFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }
}