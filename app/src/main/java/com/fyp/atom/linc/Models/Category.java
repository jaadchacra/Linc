package com.fyp.atom.linc.Models;

import java.io.Serializable;

/**
 * Created by ATOM on 11/16/2016.
 */

public class Category implements Serializable {
    private String categoryName;
    private String drawableName;
    private int mId;


    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }


    public String getDrawableName() {
        return drawableName;
    }

    public void setDrawableName(String drawableName) {
        this.drawableName = drawableName;
    }


}
