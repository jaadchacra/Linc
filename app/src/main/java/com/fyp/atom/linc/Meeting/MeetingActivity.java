package com.fyp.atom.linc.Meeting;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.fyp.atom.linc.Login.LoginActivity;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.SQLiteHandler;
import com.fyp.atom.linc.Utils.SessionManager;
import com.peak.salut.Callbacks.SalutCallback;
import com.peak.salut.Callbacks.SalutDataCallback;
import com.peak.salut.Callbacks.SalutDeviceCallback;
import com.peak.salut.Salut;
import com.peak.salut.SalutDataReceiver;
import com.peak.salut.SalutDevice;
import com.peak.salut.SalutServiceData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MeetingActivity extends AppCompatActivity implements SalutDataCallback, View.OnClickListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    String offerID;
    String userID;
    String userName;
    String isTeacher;
    String myName;

    NearbyFragment nearby;
    ChatFragment chat;

    public static final String TAG = "SalutTestApp";
    public SalutDataReceiver dataReceiver;
    public SalutServiceData serviceData;
    public Salut network;

    SalutDataCallback callback;
    boolean isClicked = false;
    boolean turnOffForever = false;

    private SessionManager session;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting);

        init();

        initNearby();

    }

    private void initNearby() {
         /*Create a data receiver object that will bind the callback
        with some instantiated object from our app. */
        dataReceiver = new SalutDataReceiver(this, this);


        /*Populate the details for our awesome service. */
        serviceData = new SalutServiceData("sas", 50489, "Jad");

        /*Create an instance of the Salut class, with all of the necessary data from before.
        * We'll also provide a callback just in case a device doesn't support WiFi Direct, which
        * Salut will tell us about before we start trying to use methods.*/
        network = new Salut(dataReceiver, serviceData, new SalutCallback() {
            @Override
            public void call() {
                // wiFiFailureDiag.show();
                // OR
                Log.e(TAG, "Sorry, but this device does not support WiFi Direct.");
            }
        });
    }

    private void init() {
        session = new SessionManager(this);

        db = new SQLiteHandler(this);
        HashMap<String, String> hashMap = db.getUserDetails();
        myName = hashMap.get("name");

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        Intent mIntent = getIntent();
        offerID = mIntent.getExtras().getString("offerID");
        userID = mIntent.getExtras().getString("userID");
        userName = mIntent.getExtras().getString("userName");
        isTeacher = mIntent.getExtras().getString("isTeacher");

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        new CountDownTimer(3600000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                session.setLogin(false);
                db.deleteUsers();
                Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();
    }


    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putString("offerID", offerID);
        bundle.putString("userID", userID);
        bundle.putString("userName", userName);
        bundle.putString("isTeacher", isTeacher);

        nearby = new NearbyFragment();
        nearby.setArguments(bundle);
        adapter.addFragment(nearby, "Nearby");

        Bundle bundle1 = new Bundle();
        bundle1.putString("offerID", offerID);
        bundle1.putString("userID", userID);
        bundle.putString("userName", userName);
        bundle.putString("isTeacher", isTeacher);

        chat = new ChatFragment();
        chat.setArguments(bundle1);
        adapter.addFragment(chat, "Chat");

        viewPager.setAdapter(adapter);
    }



    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    //HOST
    private void setupNetwork()
    {
        Log.d(TAG, "setupNetwork "+ "is Host " + network.isRunningAsHost);
        if(isClicked == false)
        {
            try {
                Log.d(TAG, "setupNetwork "+ "if");
                //When a device connects and is successfully registered, this callback will be fired.
                //You can access the entire list of registered clients using the field registeredClients
                network.startNetworkService(new SalutDeviceCallback() {
                    @Override
                    public void call(SalutDevice salutDevice) {
//                        Toast.makeText(getApplicationContext(), "Device: " + salutDevice.instanceName + " connected.", Toast.LENGTH_SHORT).show();
                    }
                });

                nearby.tutorButtonStarted();


                isClicked = true;

            } catch (Exception e){
//                Toast.makeText(this, "Don't click the button very fast please", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Log.d(TAG, "setupNetwork "+ "else");
            try {

                nearby.tutorButtonStopped();

                network.stopNetworkService(false);

                isClicked = false;

            } catch (Exception e){
//                Toast.makeText(this, "Don't click the button very fast please", Toast.LENGTH_SHORT).show();
            }

        }
    }

    //CLIENT
    private void discoverServices()
    {
        Log.d(TAG, "discoverServices "+ "is discoverServices is host" + network.isRunningAsHost +" & is Discovering "+ network.isDiscovering);
        if(!network.isRunningAsHost && !network.isDiscovering)
        {
            Log.d(TAG, "discoverServices "+ "if");

            //Salut will only connect to found services of the same type.
            try {
                network.discoverNetworkServices(new SalutCallback() {
                    @Override
                    public void call() {
                        Toast.makeText(getApplicationContext(), "Device: " + network.foundDevices.get(0).instanceName + " found.", Toast.LENGTH_SHORT).show();
                        //TODO: set text to tell them turn it off, put boolean to keep it off forever
                        turnOffForever = true;
                        nearby.clickStudentBtn();


                    }
                }, true);

                nearby.studentButtonStarted();

            } catch (Exception e){
//                Toast.makeText(this, "Don't click the button very fast please", Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            Log.d(TAG, "discoverServices "+ "else");

            //the framework will continue to discover services until you manually call stopServiceDiscovery()
            try {
                nearby.studentButtonStopped(turnOffForever);
                network.stopServiceDiscovery(true);

            } catch (Exception e){
//                Toast.makeText(this, "Don't click the button very fast please", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onDataReceived(Object o) {
        //Data Is Received
        Toast.makeText(getApplicationContext(), "Data is received", Toast.LENGTH_SHORT).show();
    }

    public void tutorPressed(){
        if(!Salut.isWiFiEnabled(getApplicationContext()))
        {
            Toast.makeText(getApplicationContext(), "Please enable WiFi first.", Toast.LENGTH_SHORT).show();
            return;
        }

        setupNetwork();
    }

    public void studentPressed(){
        if(!Salut.isWiFiEnabled(getApplicationContext()))
        {
            Toast.makeText(getApplicationContext(), "Please enable WiFi first.", Toast.LENGTH_SHORT).show();
            return;
        }
        discoverServices();
    }
}