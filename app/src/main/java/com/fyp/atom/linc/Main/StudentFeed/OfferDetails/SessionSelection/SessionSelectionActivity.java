package com.fyp.atom.linc.Main.StudentFeed.OfferDetails.SessionSelection;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Main.StudentFeed.CreateOffer.CreateOfferActivity;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.Models.Session;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Utils.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class SessionSelectionActivity extends AppCompatActivity {
    Bundle mSavedInstanceState;
    private SQLiteHandler db;
    String userID;
    Toolbar offerDetailsToolbar;
    TextView toolbarTextView;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter sessionAdapter;
    private List<Session> sessionList;
    ProgressBar progressBarCenter;
    TextView errorMessage;
    Button retryButton;
    Offer offer;
    String offerID;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                //I REMOVED THIS AND PUT (0,0) !!!, there was one also in onCreate for when we enter the app
//                this.overridePendingTransition(R.anim.animation_leave2,R.anim.animation_enter2);
                overridePendingTransition(0, 0);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_session);
        mSavedInstanceState = savedInstanceState;

        init();
        initSQLHandler();

        getSessions(mSavedInstanceState);
    }

    private void init() {
        Intent mIntent = getIntent();
        offer = (Offer) mIntent.getSerializableExtra("offer");
        offerID = ""+offer.getOfferID();

        progressBarCenter = (ProgressBar) findViewById(R.id.progressBarCenter);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) findViewById(R.id.errorMessageTextView);
        retryButton = (Button) findViewById(R.id.retryButton);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        toolbarTextView = (TextView) findViewById(R.id.toolbarTextView);
        offerDetailsToolbar = (Toolbar) findViewById(R.id.offerDetailsToolbar);

        sessionList = new ArrayList<>();

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular_0.ttf");
        toolbarTextView.setTypeface(tf);
        setSupportActionBar(offerDetailsToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setSupportActionBar(offerDetailsToolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void getSessions(final Bundle savedInstanceState) {
        MySingleton.getInstance(this).addToRequestQueue(getSessionsFromServer(savedInstanceState));
    }
    private StringRequest getSessionsFromServer(final Bundle savedInstanceState) {
        //Displaying Progressbar
        progressBarCenter.setVisibility(View.VISIBLE);

        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, "http://kcapplications.com/Linc/sessions.php?user_id="+userID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("sessions", response.toString());
                        if (response != null) {
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            getSessions(savedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("sessions", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                getSessions(savedInstanceState);
                            }
                        });
                    }
                });
        return postRequest;
    }
    //This method will parse json data
    private void parseData(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                Session session = new Session();

                JSONObject sessionJsonObject = null;

                String sessionStatus = "";

                try {
                    //Getting json
                    sessionJsonObject = jsonArray.getJSONObject(i);
                    session.setSessionID(Integer.parseInt(sessionJsonObject.getString("sessionID")));
                    session.setName(sessionJsonObject.getString("sessionName"));
                    if (sessionJsonObject.getString("sessionHighestPrice") != "null")
                        session.setSessionHighestPrice(Integer.parseInt(sessionJsonObject.getString("sessionHighestPrice")));
                    sessionStatus = sessionJsonObject.getString("sessionStatus");


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(sessionStatus.equals("0"))
                    sessionList.add(0, session);
            }
//        //Notifying the adapter that data has been added or changed
            sessionAdapter = new SessionsSelectionAdapter(sessionList, this);
            recyclerView.setAdapter(sessionAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initSQLHandler() {
        // SQLite database handler
        db = new SQLiteHandler(this.getApplicationContext());
        HashMap<String, String> user = db.getUserDetails();
        userID = user.get("uid");
//        Log.d("userserserser", user.get("name")+" "+user.get("uid"));
    }

    public void addToSession(final String sessionID) {
        progressBarCenter.setVisibility(View.VISIBLE);

        //Displaying Progressbar
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://kcapplications.com/Linc/session/offer.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        progressBarCenter.setVisibility(View.GONE);

                        Log.d("responsingz", s);
                        if(!s.equals("success"))
                            Toast.makeText(SessionSelectionActivity.this, s, Toast.LENGTH_SHORT).show();
                        else{
                            Intent myIntent = new Intent(SessionSelectionActivity.this, MainActivity.class);
                            SessionSelectionActivity.this.startActivity(myIntent);
                            finish();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressBarCenter.setVisibility(View.GONE);
                        if (volleyError instanceof NoConnectionError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof NetworkError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else if (volleyError instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_LONG).show();
                        } else{
                            Toast.makeText(getApplicationContext(), volleyError.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                //Adding parameters
                params.put("session_id", sessionID);
                params.put("offer_id", offerID);
                params.put("user_id", userID);
                return params;
            }
        };
        MySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
