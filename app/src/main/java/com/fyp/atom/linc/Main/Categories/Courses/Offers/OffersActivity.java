package com.fyp.atom.linc.Main.Categories.Courses.Offers;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fyp.atom.linc.Models.Category;
import com.fyp.atom.linc.Models.Tag;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Models.Course;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Models.User;
import com.fyp.atom.linc.Utils.SQLiteHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class OffersActivity extends AppCompatActivity {
    String courseName = "";
    String categoryName = "";
    ProgressBar progressBarCenter;
    TextView errorMessage, toolbarText, noOffersMessage;
    Button retryButton;
    Toolbar toolbar;
    List<Offer> offersList;
    Bundle globalSavedInstanceState;
    private RecyclerView.Adapter adapter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private SQLiteHandler db;
    int myID;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                //I REMOVED THIS AND PUT (0,0) !!!, there was one also in onCreate for when we enter the app
//                this.overridePendingTransition(R.anim.animation_leave2,R.anim.animation_enter2);
                overridePendingTransition(0, 0);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        globalSavedInstanceState = savedInstanceState;
        Log.d("ActivityNow", "I'm in OffersActivity!");

        offersList = new ArrayList<>();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_offers);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        progressBarCenter = (ProgressBar) findViewById(R.id.progressbar_center_offers);
        progressBarCenter.setVisibility(View.VISIBLE);
        errorMessage = (TextView) findViewById(R.id.error_message_offers);
        retryButton = (Button) findViewById(R.id.retry_button_offers);
        errorMessage.setVisibility(View.GONE);
        retryButton.setVisibility(View.GONE);

        noOffersMessage = (TextView) findViewById(R.id.no_offers);
        noOffersMessage.setVisibility(View.GONE);

        // SQLite database handler
        db = new SQLiteHandler(this);
        HashMap<String, String> hashMap = db.getUserDetails();
        //Looping through hashmap to see values
        //        for (String key : hashMap.keySet()) {
        //            Log.d("hahahsgsgs","" + key);
        //            Log.d("hahahsgsgs","" + hashMap.get(key));
        //        }
        myID = Integer.parseInt(hashMap.get("uid"));

        toolbarText = (TextView) findViewById(R.id.text_toolbar_offers);
        //initializing our adapter
        adapter = new OffersAdapter(offersList, this);

        //Adding adapter to recyclerview
        recyclerView.setAdapter(adapter);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/montserrat_regular_0.ttf");
        errorMessage.setTypeface(tf);
        retryButton.setTypeface(tf);
        toolbarText.setTypeface(tf);

        Intent mIntent = getIntent();
        courseName = mIntent.getExtras().getString("courseName");
        toolbarText.setText(courseName);
//        Log.d("offerrs", courseName);

        if (mIntent.getExtras().getString("categoryName") != null)
            categoryName = mIntent.getExtras().getString("categoryName");
        else
            categoryName = "random";


        String categoryBarImgName = categoryName;
        categoryBarImgName = categoryBarImgName.toLowerCase() + "_bar";
        Log.d("aaaa", categoryBarImgName);
        int resId = getResources().getIdentifier(categoryBarImgName, "drawable", getPackageName());
        toolbar = (Toolbar) findViewById(R.id.toolbar_offers);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setBackgroundResource(resId);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        try {
            getOffers(globalSavedInstanceState);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//
    }

    public String getCourseName(){
        return courseName;
    }
///////////////////////////////////////////////////////////////////////////////////
    private void getOffers(final Bundle savedInstanceState) throws UnsupportedEncodingException {
        MySingleton.getInstance(this).addToRequestQueue(getOffersFromServer(savedInstanceState));
    }
    private StringRequest getOffersFromServer(final Bundle savedInstanceState) throws UnsupportedEncodingException {
        //Displaying Progressbar
        progressBarCenter.setVisibility(View.VISIBLE);
        String myDate = dateStrNow();
        myDate = myDate.replaceAll(" ","%");//URLEncoder.encode(dateStrNow(), "utf-8");;
        String mycourseName = URLEncoder.encode(courseName, "utf-8");;
        String url = "http://kcapplications.com/Linc/getOffers.php?" + "course=" + mycourseName+"&date="+myDate+"&user_id="+myID;
        Log.d("offerActivityjadofferrs", url);

        //JsonArrayRequest of volley
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBarCenter.setVisibility(View.GONE);
                        Log.d("offerActivityjadofferrs", response.toString());
                        if (response != null) {
                            parseData(response);
                        } else {
                            progressBarCenter.setVisibility(View.VISIBLE);
                            try {
                                getOffers(savedInstanceState);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBarCenter.setVisibility(View.GONE);
                        errorMessage.setVisibility(View.VISIBLE);
                        retryButton.setVisibility(View.VISIBLE);
                        Log.d("offerrs", "in error");

                        if (error instanceof NoConnectionError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof NetworkError) {
                            errorMessage.setText("Connection error\nPlease check your internet connection");
                        } else if (error instanceof TimeoutError) {
                            errorMessage.setText("Connection error\nFailed to connect to server");
                        } else {
                            errorMessage.setText("404 Not Found");

                        }
                        retryButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                progressBarCenter.setVisibility(View.VISIBLE);
                                errorMessage.setVisibility(View.GONE);
                                retryButton.setVisibility(View.GONE);
                                try {
                                    getOffers(savedInstanceState);
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
        return postRequest;
    }
    private String dateStrNow() {
        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowStr = sdf.format(now.getTime());
        return nowStr;
    }
    //This method will parse json data
    private void parseData(String response) {
        try {
            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i < jsonArray.length(); i++) {
                Offer offer = new Offer();
                User user = new User();
                Course course = new Course();
                Category category = new Category();

                offer.setUser(user);
                offer.setCourse(course);
                offer.setCategory(category);

                JSONObject offerJsonObject = null;
                JSONObject userJsonObject = null;
                JSONObject categoryJsonObject = null;
                JSONArray tagJsonArray = null;

                String userID = "";

                try {
                    Log.d("offerActivityjadofferrs", "in parse");
                    Log.d("offerActivityjadofferrs", "in parse2");

                    //Getting json
                    offerJsonObject = jsonArray.getJSONObject(i);
                    userJsonObject = offerJsonObject.getJSONObject("User");
                    categoryJsonObject = offerJsonObject.getJSONObject("Category");;
                    tagJsonArray = offerJsonObject.getJSONArray("Tag");
                    ArrayList<Tag> tagsList = new ArrayList<>();
                    if (tagJsonArray != null) {
                        for (int j=0;j<tagJsonArray.length();j++){
                            Tag tag = new Tag();
                            tag.setName(tagJsonArray.getJSONObject(j).getString("tagName"));
                            tagsList.add(tag);
                            Log.d("offerActivityjadofferrs", tag.getName());
                        }
                    }

                    Log.d("offerActivityjadofferrs", "in parse2");

//                  Getting the offer

                    offer.setOfferID(Integer.parseInt(offerJsonObject.getString("offerID")));
                    offer.setTitle(offerJsonObject.getString("offerTitle"));
                    offer.getCourse().setName(offerJsonObject.getString("Course"));
                    offer.setDescription(offerJsonObject.getString("offerDescription"));
                    offer.setPrice(offerJsonObject.getInt("offerPrice"));

                    String dateStr = offerJsonObject.getString("offerStartDate");
                    offer.setDateStr(getDateStr(offer, dateStr));

                    offer.setTagsList(tagsList);

                    userID = userJsonObject.getString("userID");
                    offer.getUser().setUserEmail(userJsonObject.getString("userEmail"));
                    offer.getUser().setUserFirstName(userJsonObject.getString("userFirstName"));
                    offer.getUser().setUserLastName(userJsonObject.getString("userLastName"));
                    offer.getUser().setUserRating(userJsonObject.getInt("userRating"));
                    offer.getUser().setUserResponse(userJsonObject.getInt("userResponse"));
                    offer.getUser().setUserReliability(userJsonObject.getInt("userReputation"));


                    offer.getCategory().setCategoryName(categoryJsonObject.getString("categoryName"));



                    Log.d("offerActivityjadofferrs", offer.getCategory().getCategoryName());

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String myuid = ""+myID;
                if(!userID.equals(myuid)) {
                    Log.d("useridididid", myID + " server: "+ userID);
                    offersList.add(i, offer);
                }
            }
//
//        //Notifying the adapter that data has been added or changed
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String getDateStr(Offer offer, String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = sdf.parse(dateStr);
        offer.setOfferStartDate(startDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        SimpleDateFormat formatter = new SimpleDateFormat("EEE");
        String dayOfWeekStr = formatter.format(cal.getTime());
        int startHour = cal.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
        String startHourStr = String.format("%02d", startHour);

        int startMinute = cal.get(Calendar.MINUTE);
        String startMinuteStr = String.format("%02d", startMinute);

        int endHour = startHour + 1;
        if(endHour == 25)
            endHour = 0;
        String endHourStr = String.format("%02d", endHour);

        String offerDate =  dayOfWeekStr+" from "+startHourStr+":"+startMinuteStr+" till "+endHourStr+":"+startMinuteStr;
        return offerDate;
    }

}
