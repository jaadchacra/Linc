package com.fyp.atom.linc.Main.Sessions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fyp.atom.linc.Main.Sessions.SessionDetails.SessionDetailsActivity;
import com.fyp.atom.linc.Models.Session;
import com.fyp.atom.linc.R;

import java.util.List;


public class SessionsAdapter extends RecyclerView.Adapter<SessionsAdapter.ViewHolder> {

    private Context context;

//    ImageButton retryButtonDiscount = null;

    //List to store all superheroes
    List<Session> sessionList;

    //Constructor of this class
    public SessionsAdapter(List<Session> offersList, Context context) {
        super();
        this.sessionList = offersList;
        this.context = context;
        Log.d("offer", "StudentFeedAdapter");

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_item_session, parent, false);

        Log.d("offer", "yo");
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //Getting the particular item from the list
        final Session session = sessionList.get(position);

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(context, SessionDetailsActivity.class);
                myIntent.putExtra("session", session);
                context.startActivity(myIntent);
                ((Activity)context).overridePendingTransition(0, 0);
            }
        });

        //Grg said no need for delete now
//        deleteSessionClickListener(holder);
        holder.sessionNameTextView.setText(session.getName());
        holder.priceTextView.setText(""+session.getSessionHighestPrice());


    }

    @Override
    public int getItemCount() {
        return sessionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public TextView sessionNameTextView;
        public TextView priceTextView;
//        public ImageView closeButtonImageView;
        public CardView card;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            sessionNameTextView = (TextView) itemView.findViewById(R.id.sessionNameTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.priceTextView);
//            closeButtonImageView = (ImageView) itemView.findViewById(R.id.closeButtonImageView);
            card = (CardView) itemView.findViewById(R.id.card);
        }
    }

    private void deleteSessionClickListener(ViewHolder holder) {
//        holder.closeButtonImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setTitle("Delete Session")
//                        .setMessage("Are you sure you want to delete this session?")
//                        .setCancelable(true)
//                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                // TODO: Call function to Delete the session
//                                dialog.cancel();
//                            }
//                        }).setPositiveButton("Delete", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        // TODO: Call function to Delete the session AND Notify change for adapter
//                        dialog.cancel();
//                    }
//                });
//                AlertDialog alert = builder.create();
//                alert.show();
//            }
//        });
    }
}