package com.fyp.atom.linc.Main.Profile;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fyp.atom.linc.Login.LoginActivity;
import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Main.Profile.MyOffers.MyOffers;
import com.fyp.atom.linc.Main.StudentFeed.OfferDetails.SessionSelection.SessionSelectionActivity;
import com.fyp.atom.linc.Main.StudentFeed.StudentFeedAdapter;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.R;
import com.fyp.atom.linc.Utils.SQLiteHandler;
import com.fyp.atom.linc.Utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

public class ProfileFragment extends Fragment {
    View view;
    Bundle mSavedInstanceState;
    private SessionManager session;
    private SQLiteHandler db;
    TextView availableCreditTextView, reliabilityTextView, responseTimeTextView, nameTextView;
    ImageView profileImageView,
            starOneImageView, starTwoImageView, starThreeImageView, starFourImageView, starFiveImageView,
            fb, insta, twitter;
    Button offersButton, logoutButton;
    String userFacebook, userTwitter, userInstagram;
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (isVisibleToUser) {

            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        //This is basically never used, it's just to use getData from onScrolled function
        mSavedInstanceState = savedInstanceState;

        init();



        offersButtonClickListener();

        logoutButtonClickListener();


        socialMediaClickListeners();

        return view;
    }

    private void logoutButtonClickListener() {
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session.setLogin(false);
                db.deleteUsers();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    private void init() {
        // Session manager
        session = new SessionManager(getActivity());

        db = new SQLiteHandler(getActivity());
        HashMap<String, String> hashMap = db.getUserDetails();

        profileImageView = (ImageView) view.findViewById(R.id.profileImageView);
        starOneImageView = (ImageView) view.findViewById(R.id.starOneImageView);
        starTwoImageView = (ImageView) view.findViewById(R.id.starTwoImageView);
        starThreeImageView = (ImageView) view.findViewById(R.id.starThreeImageView);
        starFourImageView = (ImageView) view.findViewById(R.id.starFourImageView);
        starFiveImageView = (ImageView) view.findViewById(R.id.starFiveImageView);
        fb = (ImageView) view.findViewById(R.id.fb);
        insta = (ImageView) view.findViewById(R.id.insta);
        twitter = (ImageView) view.findViewById(R.id.twitter);


        availableCreditTextView = (TextView) view.findViewById(R.id.availableCreditTextView);
        reliabilityTextView = (TextView) view.findViewById(R.id.reliabilityTextView);
        responseTimeTextView = (TextView) view.findViewById(R.id.responseTimeTextView);
        nameTextView = (TextView) view.findViewById(R.id.nameTextView);

        offersButton = (Button) view.findViewById(R.id.offersButton);
        logoutButton = (Button) view.findViewById(R.id.logoutButton);

        String userEmail = hashMap.get("email");
        String logoUrl = "http://kcapplications.com/Linc/images/" + userEmail +"_logo.jpg";
        int availableCredit = Integer.parseInt(hashMap.get("credit"));
        int rating = Integer.parseInt(hashMap.get("userRating"));
        String responseTime = "reponse time: " + hashMap.get("userResponse")+"h";
        String reliability = "reliability: "+hashMap.get("userReputation")+"%";
        String name = hashMap.get("name") + " ("+userEmail.substring(0,5)+")";
        userFacebook = hashMap.get("userFacebook");
        userTwitter = hashMap.get("userTwitter");
        userInstagram = hashMap.get("userInstagram");

        Log.d("socialmedzz", "sc"+userFacebook);
        Log.d("socialmedzz", "sc"+userTwitter);
        Log.d("socialmedzz", "sc"+userInstagram);

        if(userFacebook.equals(""))
            fb.setVisibility(View.GONE);
        if(userTwitter.equals(""))
            twitter.setVisibility(View.GONE);
        if(userInstagram.equals(""))
            insta.setVisibility(View.GONE);

        availableCreditTextView.setText(""+availableCredit);
        nameTextView.setText(name);
        reliabilityTextView.setText(reliability);
        responseTimeTextView.setText(responseTime);

        Picasso.with(getActivity())
                .load(logoUrl)
                .placeholder(R.drawable.placeholder)
                .into(profileImageView);

        setRatingImage(rating);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    public void setRatingImage(int rating){
        String uriStarOn = "@drawable/star_on";  // where myresource (without the extension) is the file
        int imageResourceOn = getActivity().getResources().getIdentifier(uriStarOn, null, getActivity().getPackageName());
        Drawable starOn = getActivity().getResources().getDrawable(imageResourceOn);

        String uriStarOff = "@drawable/star_off";  // where myresource (without the extension) is the file
        int imageResourceOff = getActivity().getResources().getIdentifier(uriStarOff, null, getActivity().getPackageName());
        Drawable starOff = getActivity().getResources().getDrawable(imageResourceOff);

        if(rating == 0){
            starOneImageView.setImageDrawable(starOff);
            starTwoImageView.setImageDrawable(starOff);
            starThreeImageView.setImageDrawable(starOff);
            starFourImageView.setImageDrawable(starOff);
            starFiveImageView.setImageDrawable(starOff);
        } else if(rating == 1){
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOff);
            starThreeImageView.setImageDrawable(starOff);
            starFourImageView.setImageDrawable(starOff);
            starFiveImageView.setImageDrawable(starOff);
        } else if(rating == 2){
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOn);
            starThreeImageView.setImageDrawable(starOff);
            starFourImageView.setImageDrawable(starOff);
            starFiveImageView.setImageDrawable(starOff);
        }else if(rating == 3){
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOn);
            starThreeImageView.setImageDrawable(starOn);
            starFourImageView.setImageDrawable(starOff);
            starFiveImageView.setImageDrawable(starOff);
        }else if(rating == 4){
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOn);
            starThreeImageView.setImageDrawable(starOn);
            starFourImageView.setImageDrawable(starOn);
            starFiveImageView.setImageDrawable(starOff);
        }else if(rating == 4){
            starOneImageView.setImageDrawable(starOn);
            starTwoImageView.setImageDrawable(starOn);
            starThreeImageView.setImageDrawable(starOn);
            starFourImageView.setImageDrawable(starOn);
            starFiveImageView.setImageDrawable(starOn);
        }
    }

    private void offersButtonClickListener() {
        offersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(getActivity(), MyOffers.class);
                startActivity(myIntent);
            }
        });
    }
    private void socialMediaClickListeners() {
        fb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    // TODO REMINDER: since we have the textView changed to fb.com instead of www.fb.com we get the first index of '.'
                    int versionCode = getActivity().getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
                    if (versionCode >= 3002850) {
                        Uri uri = Uri.parse("fb://facewebmodal/f?href=" + "http://www.facebook.com/" + userFacebook.substring(userFacebook.indexOf(".com") + 5));
                        startActivity(new Intent(Intent.ACTION_VIEW, uri));
                    } else {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + userFacebook.substring(userFacebook.indexOf(".com") + 5))));
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    try{
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.facebook.com/" + userFacebook.substring(userFacebook.indexOf(".com") + 5))));
                    }catch (Exception er){
                        Toast.makeText(getActivity(), "Invalid Url provided", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int subStr = userTwitter.length();

                if (userTwitter.contains("?"))
                    subStr = userTwitter.indexOf("?");

                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("twitter://user?screen_name=" + userTwitter.substring(userTwitter.indexOf(".com") + 5, subStr)));
                    startActivity(intent);

                } catch (Exception e) {
                    try{
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/" + userTwitter.substring(userTwitter.indexOf(".com") + 5, subStr))));
                    }catch (Exception er){
                        Toast.makeText(getActivity(), "Invalid Url provided", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        insta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://instagram.com/_u/" + userInstagram.substring(userInstagram.indexOf(".com") + 5)));
                    startActivity(intent);

                } catch (Exception e) {
                    try{
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/" + userInstagram.substring(userInstagram.indexOf(".com") + 5))));
                    }catch (Exception er){
                        Toast.makeText(getActivity(), "Invalid Url provided", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void setCredits(String credit){
        availableCreditTextView.setText(credit);
    }
}
