package com.fyp.atom.linc.Main.Profile.MyOffers.MyOffersDetails;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Main.StudentFeed.OfferDetails.SessionSelection.SessionSelectionActivity;
import com.fyp.atom.linc.Models.Approval;
import com.fyp.atom.linc.Models.Requester;
import com.fyp.atom.linc.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ATOM on 11/30/2016.
 */

public class RequestersAdapter extends RecyclerView.Adapter<RequestersAdapter.ViewHolder> {
    private Context context;
    List<Requester> requestersList;

    //Constructor of this class
    public RequestersAdapter(List<Requester> requestersList, Context context) {
        super();
        this.requestersList = requestersList;
        this.context = context;
    }

    @Override
    public RequestersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_requesters, parent, false);

        RequestersAdapter.ViewHolder viewHolder = new RequestersAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RequestersAdapter.ViewHolder holder, final int position) {
        //Getting the particular item from the list
        final Requester requester = requestersList.get(position);

//        String offeringCredit = requester.getOfferingCredit();
//        holder.priceTextView.setText(offeringCredit);


        String requesterName = requester.getRequester().getName();
        holder.textApproval.setText(requesterName);
        Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/montserrat_regular_0.ttf");
        holder.textApproval.setTypeface(tf);

        String logoUrl = "http://kcapplications.com/Linc/images/" + requester.getRequester().getUserEmail() +"_logo.jpg";

        Log.d("logoooo", logoUrl);
        Picasso.with(context)
                .load(logoUrl)
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewApproval);

        holder.approveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RequestersActivity)context).approve(""+requester.getRequester().getUserID());
            }
        });

    }

    @Override
    public int getItemCount() {
        return requestersList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public Button approveButton;
        public CircleImageView imageViewApproval;
        public TextView textApproval;
        public TextView priceTextView;
        public TextView titleOfferingTextView;
        public ImageView priceImageView;




        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);
            approveButton = (Button) itemView.findViewById(R.id.approveButton);
            imageViewApproval = (CircleImageView) itemView.findViewById(R.id.imageViewApproval);
            textApproval = (TextView) itemView.findViewById(R.id.textApproval);
            priceTextView = (TextView) itemView.findViewById(R.id.priceTextView);
            titleOfferingTextView = (TextView) itemView.findViewById(R.id.priceTextView);
            priceImageView = (ImageView) itemView.findViewById(R.id.priceImageView);

            priceTextView.setVisibility(View.GONE);
            titleOfferingTextView.setVisibility(View.GONE);
            priceImageView.setVisibility(View.GONE);


        }
    }

}
