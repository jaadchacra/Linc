package com.fyp.atom.linc.Main.StudentFeed.CreateOffer.OfferImage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.fyp.atom.linc.Main.StudentFeed.CreateOffer.CreateOfferActivity;
import com.fyp.atom.linc.Models.Offer;
import com.fyp.atom.linc.R;
import com.lyft.android.scissors.CropView;

import java.io.File;

import rx.Observable;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

import static android.graphics.Bitmap.CompressFormat.JPEG;
import static rx.android.schedulers.AndroidSchedulers.mainThread;
import static rx.schedulers.Schedulers.io;

public class OfferImageCropActivity extends Activity {

    public static final int PICK_IMAGE_FROM_GALLERY = 10001;
    public static final int PICK_IMAGE_FROM_CAMERA = 10002;


    CropView cropView;
    Context context;
    CompositeSubscription subscriptions = new CompositeSubscription();
    Offer offer;

    Button pickButton, sendButton, cancelButton;

    private int selectedRatio = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_offer_image_crop);
        context = this;

        init();

        cropViewTouchListener();

        sendButtonClickListener();

        cancelButtonClickListener();

        cropView.extensions().pickUsing(OfferImageCropActivity.this, PICK_IMAGE_FROM_GALLERY);
        pickButton.setVisibility(View.GONE);
    }

    private void init() {
        offer = (Offer) getIntent().getSerializableExtra("offer");

        cropView = (CropView) findViewById(R.id.crop_view);
        pickButton = (Button) findViewById(R.id.pick_button);
        sendButton = (Button) findViewById(R.id.send_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);

        sendButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);
    }

    private void cropViewTouchListener() {
        cropView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getPointerCount() > 1 || cropView.getImageBitmap() == null) {
                    return true;
                }
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_MOVE:
                        cancelButton.setVisibility(View.GONE);
                        sendButton.setVisibility(View.GONE);
                        break;
                    default:
                        cancelButton.setVisibility(View.VISIBLE);
                        sendButton.setVisibility(View.VISIBLE);
                        break;
                }
                return true;
            }
        });
    }

    private void sendButtonClickListener() {
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                final File croppedFile = new File(getCacheDir(), "cropped.jpg");

                Observable<Void> onSave = Observable.from(cropView.extensions()
                        .crop()
                        .quality(100)
                        .format(JPEG)
                        .into(croppedFile))
                        .subscribeOn(io())
                        .observeOn(mainThread());

                subscriptions.add(onSave
                        .subscribe(new Action1<Void>() {
                            @Override
                            public void call(Void nothing) {
                                try {
                                    Intent intent = new Intent(OfferImageCropActivity.this, CreateOfferActivity.class);
                                    intent.putExtra("caller", "OfferImageCropActivity");
                                    intent.putExtra("offer", offer);
                                    intent.putExtra("EXTRA_FILE_PATH", croppedFile.getPath());
                                    startActivity(intent);
                                    finish();
                                    Log.d("where1", "try");
                                }catch(Exception e){
                                    Log.d("where1", "catch");
                                }
                            }
                        }));
            }
        });
    }

    private void cancelButtonClickListener() {
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                backIntent();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri galleryPictureUri = data.getData();

            cropView.extensions().load(galleryPictureUri);

            cancelButton.setVisibility(View.VISIBLE);
            sendButton.setVisibility(View.VISIBLE);
        } else if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == Activity.RESULT_CANCELED) {
            backIntent();
        } else if (requestCode == PICK_IMAGE_FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            //TODO: implement OfferPicModeESelectDialogFragment in CreateOfferActivity, if Camera, go to another Activity (CameraActivity) then send bitmap to this Activity to crop it (Lyft ddnt implement camera)
        } else if (requestCode == PICK_IMAGE_FROM_CAMERA && resultCode == Activity.RESULT_CANCELED) {
            backIntent();
        } else {
            errored();
        }
    }

    private void backIntent(){
        Intent intent = new Intent(OfferImageCropActivity.this, CreateOfferActivity.class);
        intent.putExtra("caller", "OfferImageCropActivity");
        intent.putExtra("offer", offer);
        intent.putExtra("EXTRA_FILE_PATH", "");
        startActivity(intent);
        finish();
    }

    public void errored() {
        Intent intent = new Intent();
        intent.putExtra("error", true);
        if ("Error while opening the image file. Please try again." != null) {
            intent.putExtra("error_msg", "Error while opening the image file. Please try again.");
        }
        finish();
    }
}
