package com.fyp.atom.linc.Main.StudentFeed.OfferDetails.SessionSelection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fyp.atom.linc.Main.MainActivity;
import com.fyp.atom.linc.Models.Session;
import com.fyp.atom.linc.R;

import java.util.List;


public class SessionsSelectionAdapter extends RecyclerView.Adapter<SessionsSelectionAdapter.ViewHolder> {

    private Context context;

//    ImageButton retryButtonDiscount = null;

    //List to store all superheroes
    List<Session> sessionList;

    //Constructor of this class
    public SessionsSelectionAdapter(List<Session> offersList, Context context) {
        super();
        this.sessionList = offersList;
        this.context = context;
        Log.d("sessionSelection", "sessionSelectionAdapter");

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.list_item_session_selection, parent, false);

        Log.d("sessionSelection", "yo");
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        //Getting the particular item from the list
        final Session session = sessionList.get(position);

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SessionSelectionActivity)context).addToSession(""+session.getSessionID());
            }
        });

        holder.sessionNameTextView.setText(session.getName());
        holder.priceTextView.setText(""+session.getSessionHighestPrice());

    }

    @Override
    public int getItemCount() {
        return sessionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        //Views
        public CardView card;
        public TextView sessionNameTextView;
        public TextView priceTextView;

        //Initializing Views
        public ViewHolder(View itemView) {
            super(itemView);

            card = (CardView) itemView.findViewById(R.id.card);
            sessionNameTextView = (TextView) itemView.findViewById(R.id.sessionNameTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.priceTextView);

        }
    }
}