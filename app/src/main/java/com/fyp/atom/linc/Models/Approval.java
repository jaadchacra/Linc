package com.fyp.atom.linc.Models;

import java.io.Serializable;

/**
 * Created by ATOM on 4/26/2017.
 */

public class Approval implements Serializable {
    User user;
    String dateStr;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }
}
