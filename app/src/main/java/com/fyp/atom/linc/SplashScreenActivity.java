package com.fyp.atom.linc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.fyp.atom.linc.Login.LoginActivity;
import com.fyp.atom.linc.Utils.MySingleton;
import com.fyp.atom.linc.Utils.TinyDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class SplashScreenActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;
    Bundle globalSavedInstanceState;
    TextView errorText;
    TinyDB tinyDB;
    ArrayList<String> allCoursesList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        errorText = (TextView) findViewById(R.id.errorText);
        errorText.setVisibility(View.GONE);

        tinyDB = new TinyDB(this);
        allCoursesList = new ArrayList<String>();


        getAllCourses(globalSavedInstanceState);

    }

    private void getAllCourses(final Bundle savedInstanceState) {
        MySingleton.getInstance(this).addToRequestQueue(getAllCoursesFromServer(savedInstanceState));
    }

    private JsonArrayRequest getAllCoursesFromServer(final Bundle savedInstanceState) {

        //JsonArrayRequest of volley
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("http://kcapplications.com/Linc/allcourses.php",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("allCourses", "1"+response.toString());
                        if(response != null){
                            parseData(response);
                            Intent myIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                            myIntent.putExtra("isFailed", true);
                            startActivity(myIntent);
                            finish();
                        } else {
                            getAllCourses(globalSavedInstanceState);
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorText.setVisibility(View.VISIBLE);
                        getAllCourses(globalSavedInstanceState);
                    }
                });
        return jsonArrayRequest;
    }

    //This method will parse json data about courses and put them in tinyDB
    private void parseData(JSONArray array) {
        for (int i = 0; i < array.length(); i++) {
            String course = "";
            JSONObject json = null;
            try {
                //Getting json
                json = array.getJSONObject(i);
//                //Getting the course
                course = json.getString("courseName");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            allCoursesList.add(i, course);
        }
//
//        //Notifying the adapter that data has been added or changed
        Log.d("allCourses", "2"+allCoursesList.toString());
        tinyDB.putListString("allCourses", allCoursesList);

    }

}